package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAILeapAtTarget;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(EntityAILeapAtTarget.class)
public abstract class LeapAtTargetMixin {

    @Shadow
    private EntityLiving leaper;

    @Inject(method = "shouldContinueExecuting", at = @At("HEAD"), cancellable = true)
    private void onShouldContinueExecuting(CallbackInfoReturnable<Boolean> cir) {
        EntityLivingBase entitylivingbase = this.leaper.getAttackTarget();
        if (entitylivingbase == null) cir.setReturnValue(false);
    }
}
