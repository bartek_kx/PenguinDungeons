package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.monster.EntityGuardian;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(EntityGuardian.class)
public interface EntityGuardianAccessor {

    @Invoker("setTargetedEntity")
    public void setTargetedEntity(int entityId);
}
