package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;

import java.util.Optional;

import org.spongepowered.api.entity.living.animal.Pig;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

@Mixin(EntityPig.class)
public abstract class EntityPigMixin extends EntityAnimal {

    public EntityPigMixin(World worldIn) {
        super(worldIn);
    }

    private boolean isPenguinDungeonMob()
    {
        Pig pig = (Pig) this;
        final Optional<ResourceKey> id = pig.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        return id.isPresent();
    }

    @Inject(method = "canBeSteered", at = @At("HEAD"), cancellable = true)
    public void onCanBeSteered(CallbackInfoReturnable<Boolean> cir) {
        if (isPenguinDungeonMob())
        {
            Entity entity = this.getControllingPassenger();
            if ((entity instanceof EntityPlayer)) {
               EntityPlayer entityplayer = (EntityPlayer)entity;
               if (entityplayer.getHeldItemMainhand().getItem() == Items.CARROT_ON_A_STICK ||
                   entityplayer.getHeldItemOffhand().getItem() == Items.CARROT_ON_A_STICK) {
                   // don't allow riding with carrot on a stick
                   this.removePassengers();
               }
            }
            // dungeon pigs can not be steered
            cir.setReturnValue(false);
        }
    }

    @Inject(method = "onDeath", at = @At("HEAD"), cancellable = true)
    public void onOnDeath(DamageSource cause, CallbackInfo ci) {
        if (isPenguinDungeonMob()) {
            // don't drop saddle
            ((EntityPig)(Pig)this).setSaddled(false);
        }
    }

    @Override
    public boolean canPassengerSteer() {
        if (isPenguinDungeonMob()) {
            return false;
        } else {
            return super.canPassengerSteer();
        }
    }
}
