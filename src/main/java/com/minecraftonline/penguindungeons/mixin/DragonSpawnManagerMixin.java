package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.item.EntityEnderCrystal;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldServer;
import net.minecraft.world.end.DragonFightManager;
import net.minecraft.world.gen.feature.WorldGenSpikes;
import net.minecraft.world.gen.feature.WorldGenSpikes.EndSpike;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

import com.minecraftonline.penguindungeons.customentity.dragon.CrystalType;
import com.minecraftonline.penguindungeons.customentity.dragon.WorldGenSpikesAccessor;

import java.util.List;

@Mixin(targets = "net/minecraft/world/end/DragonSpawnManager$3")
public abstract class DragonSpawnManagerMixin {

    @Redirect(method = "Lnet/minecraft/world/end/DragonSpawnManager$3;process(Lnet/minecraft/world/WorldServer;Lnet/minecraft/world/end/DragonFightManager;Ljava/util/List;ILnet/minecraft/util/math/BlockPos;)V", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/gen/feature/WorldGenSpikes;setSpike(Lnet/minecraft/world/gen/feature/WorldGenSpikes$EndSpike;)V"))
    public void setSpikeProxy(WorldGenSpikes worldgenspikes, EndSpike spike, WorldServer worldIn,
            DragonFightManager manager, List<EntityEnderCrystal> crystals, int ticks, BlockPos pos) {
        String crystalName = null;
        for(EntityEnderCrystal crystal : crystals) {
            // iterate through summoning crystals to figure out what type they are
            // break if not all of them are the same custom crystal
            if (!crystal.hasCustomName()) {
                crystalName = null; // reset name
                break;
            }
            if (crystalName == null) crystalName = crystal.getCustomNameTag();
            else if (!crystalName.equals(crystal.getCustomNameTag()))
            {
                crystalName = null; // reset name
                break;
            }
        }
        worldgenspikes.setSpike(spike); // make sure spike is still set
        ((WorldGenSpikesAccessor)worldgenspikes).setCrystalType(CrystalType.fromName(crystalName));
    }
}
