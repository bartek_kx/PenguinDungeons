package com.minecraftonline.penguindungeons.mixin;

import net.minecraft.entity.monster.EntityHusk;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.world.World;

import java.util.Optional;

import org.spongepowered.api.entity.living.monster.Husk;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

@Mixin(EntityHusk.class)
public abstract class EntityHuskMixin extends EntityZombie {

    public EntityHuskMixin(World worldIn) {
        super(worldIn);
    }

    @Inject(method = "getCanSpawnHere", at = @At("HEAD"), cancellable = true)
    private void onGetCanSpawnHere(CallbackInfoReturnable<Boolean> cir) {
        Husk zombie = (Husk) this;
        final Optional<ResourceKey> id = zombie.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
        if (id.isPresent())
        {
            // bypass canSeeSky spawn restriction
            cir.setReturnValue(super.getCanSpawnHere());
        }
    }
}
