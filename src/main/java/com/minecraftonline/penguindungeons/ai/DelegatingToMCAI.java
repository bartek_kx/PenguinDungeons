package com.minecraftonline.penguindungeons.ai;

import net.minecraft.entity.ai.EntityAIBase;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.AITaskType;
import org.spongepowered.api.entity.ai.task.AbstractAITask;
import org.spongepowered.api.entity.living.Agent;

public class DelegatingToMCAI<T extends Agent> extends AbstractAITask<T> {

    private final EntityAIBase mcAITask;

    public DelegatingToMCAI(AITaskType type, EntityAIBase mcAITask) {
        super(type);
        this.mcAITask = mcAITask;
    }

    @Override
    public void start() {
        this.mcAITask.startExecuting();
    }

    @Override
    public boolean shouldUpdate() {
        return mcAITask.shouldExecute();
    }

    @Override
    public void update() {
        mcAITask.updateTask();
    }

    @Override
    public boolean continueUpdating() {
        return mcAITask.shouldContinueExecuting();
    }

    @Override
    public void reset() {
        mcAITask.resetTask();
    }

    @Override
    public boolean canRunConcurrentWith(AITask<T> other) {
        return false;
    }

    @Override
    public boolean canBeInterrupted() {
        return mcAITask.isInterruptible();
    }
}
