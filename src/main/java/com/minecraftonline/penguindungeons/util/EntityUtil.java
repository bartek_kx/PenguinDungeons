package com.minecraftonline.penguindungeons.util;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.entity.EntityArchetype;

public class EntityUtil {

    public static boolean areArchetypesEqual(EntityArchetype archetype1, EntityArchetype archetype2) {
        DataContainer dataContainer1 = archetype1.getEntityData().remove(DataQuery.of("id"));
        DataContainer dataContainer2 = archetype2.getEntityData().remove(DataQuery.of("id"));
        return archetype1.getType() == archetype2.getType() && dataContainer1.equals(dataContainer2);
    }
}
