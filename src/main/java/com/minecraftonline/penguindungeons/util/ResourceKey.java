package com.minecraftonline.penguindungeons.util;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

// This will be replaced with minecraft/sponge's own resource key in future.
public final class ResourceKey {

    /**
     * The namespace for all custom entities added by the plugin itself.
     */
    public static final String PENGUIN_DUNGEONS_NAMESPACE = "penguindungeons";
    /**
     * The namespace for all custom entities added by the plugin VIA the config.
     */
    public static final String PENGUIN_DUNGEONS_CONFIG_NAMESPACE = "pdconfig";
    /**
     * The namespace for vanilla minecraft.
     */
    public static final String MINECRAFT_NAMESPACE = "minecraft";

    private static final Pattern NAMESPACE_REGEX = Pattern.compile("[a-z_]+");

    private static final Pattern VALUE_REGEX = Pattern.compile("[a-z_/]+");

    /**
     * Attempts to resolve the inputted string to a ResourceKey.
     *
     * @param string String in the format namespace:value
     *               Missing the namespace is *not* tolerated.
     * @return A ResourceKey
     * @throws IllegalArgumentException If the string does not contain a single colon,
     *                                  or if the namespace or values are contain invalid
     *                                  characters / no characters.
     */
    public static ResourceKey resolve(final String string) throws IllegalArgumentException {
        final String[] split = string.split(":", 2);
        if (split.length == 1) {
            throw new IllegalArgumentException("Must be a namespaced id with a ':'");
        }
        return of(split[0], split[1]);
    }

    public static Optional<ResourceKey> tryResolve(final String string) {
        final String[] split = string.split(":", 2);
        String namespace;
        String value;
        if (split.length == 1) {
            // presume minecraft: prefix
            namespace = "minecraft";
            value = string;
        } else {
            namespace = split[0];
            value = split[1];
        }
        if (!NAMESPACE_REGEX.matcher(namespace).matches() || !VALUE_REGEX.matcher(value).matches()) {
            return Optional.empty();
        }
        return Optional.of(of(namespace, value));
    }

    public static ResourceKey of(final String namespace, final String id) {
        return new ResourceKey(namespace, id);
    }

    public static ResourceKey pd(final String id) {
        return of(PENGUIN_DUNGEONS_NAMESPACE, id);
    }

    public static ResourceKey pdConfig(final String id) {
        return of(PENGUIN_DUNGEONS_CONFIG_NAMESPACE, id);
    }

    public static ResourceKey minecraft(final String id) {
        return of(MINECRAFT_NAMESPACE, id);
    }

    private final String namespace;
    private final String value;

    public ResourceKey(final String namespace, final String value) {
        if (!NAMESPACE_REGEX.matcher(namespace).matches()) {
            throw new IllegalArgumentException("Invalid namespace, does not match regex: " + NAMESPACE_REGEX);
        }
        if (!VALUE_REGEX.matcher(value).matches()) {
            throw new IllegalArgumentException("Invalid value, does not match regex: " + VALUE_REGEX);
        }
        this.namespace = namespace;
        this.value = value;
    }

    public String namespace() {
        return this.namespace;
    }

    public String value() {
        return this.value;
    }

    /**
     * Converts the {@link ResourceKey} to a String in the format
     * namespace:value for serialization or display.
     * @return This resource key as a string.
     */
    public String asString() {
        return this.namespace + ":" + this.value;
    }

    public String formattedWith(final String s) {
        return this.namespace + s + value;
    }

    @Override
    public String toString() {
        return this.asString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResourceKey)) return false;
        ResourceKey that = (ResourceKey) o;
        return namespace.equals(that.namespace) && value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(namespace, value);
    }
}
