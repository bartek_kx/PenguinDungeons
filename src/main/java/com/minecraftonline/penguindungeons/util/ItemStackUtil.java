package com.minecraftonline.penguindungeons.util;

import org.spongepowered.api.item.inventory.ItemStack;

public class ItemStackUtil {

    public static boolean equalsIgnoringQuantity(ItemStack itemStack1, ItemStack itemStack2) {
        net.minecraft.item.ItemStack mcItemStack1 = (net.minecraft.item.ItemStack) (Object) itemStack1;
        net.minecraft.item.ItemStack mcItemStack2 = (net.minecraft.item.ItemStack) (Object) itemStack2;
        return net.minecraft.item.ItemStack.areItemsEqual(mcItemStack1, mcItemStack1)
                && net.minecraft.item.ItemStack.areItemStackTagsEqual(mcItemStack1, mcItemStack2);
    }
}
