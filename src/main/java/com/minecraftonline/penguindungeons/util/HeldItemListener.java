package com.minecraftonline.penguindungeons.util;

import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.entity.DestructEntityEvent;
import org.spongepowered.api.event.filter.type.Exclude;
import org.spongepowered.api.event.item.inventory.ChangeInventoryEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.entity.MainPlayerInventory;
import org.spongepowered.api.item.inventory.property.SlotIndex;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.item.inventory.transaction.SlotTransaction;
import org.spongepowered.api.item.inventory.type.CarriedInventory;

import java.util.Optional;

public abstract class HeldItemListener {

    private static final int SPONGE_HOTBAR_START = 36;
    private static final int OFFHAND_SLOT_INDEX = 45;

    public static boolean isHotbarSlot(int index) {
        return SPONGE_HOTBAR_START <= index && index < SPONGE_HOTBAR_START + 9;
    }

    public static boolean isSelected(Player player, int index) {
        MainPlayerInventory playerInventory = player.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class));
        return playerInventory.getHotbar().getSelectedSlotIndex() == (index - SPONGE_HOTBAR_START);
    }

    public abstract boolean isDesiredItem(ItemStackSnapshot itemStack);

    public abstract void onStartHolding(Player player, ItemStackSnapshot wand, HandType hand);

    public abstract void onStopHolding(Player player, ItemStackSnapshot wand, HandType hand);

    public abstract void onSwapHands(Player player);

    @Listener(order = Order.POST)
    public void onChangeHeld(ChangeInventoryEvent.Held event) {
        if (!(event.getTargetInventory() instanceof CarriedInventory)) {
            return; // Not carried by anyone, so they don't have hands, so they can't hold.
        }
        Optional<Player> optPlayer = event.getCause().first(Player.class);
        if (!optPlayer.isPresent()) {
            return;
        }
        final Player player = optPlayer.get();


        Optional<ItemStack> start = event.getOriginalSlot().peek();
        Optional<ItemStack> end = event.getFinalSlot().peek();

        if (start.isPresent() && end.isPresent() && ItemStackUtil.equalsIgnoringQuantity(start.get(), end.get())) {
            return;
        }

        HandType HAND = HandTypes.MAIN_HAND; // Always main hand since we can only change slots on the main hand.

        if (start.isPresent() && isDesiredItem(start.get().createSnapshot())) {
            onStopHolding(player, start.get().createSnapshot(), HAND);
        }

        if (end.isPresent() && isDesiredItem(end.get().createSnapshot())) {
            onStartHolding(player, end.get().createSnapshot(), HAND);
        }
    }

    @Exclude(ChangeInventoryEvent.Held.class)
    @Listener(order = Order.POST)
    public void onChangeInventory(ChangeInventoryEvent event) {
        Inventory inventory = event.getTargetInventory();
        if (!(inventory instanceof CarriedInventory)) {
            return; // Not carried by anyone, so they don't have hands, so they can't hold.
        }
        Optional<Player> optPlayer = event.getCause().first(Player.class);
        if (!optPlayer.isPresent()) {
            return;
        }
        final Player player = optPlayer.get();

        if (event instanceof ChangeInventoryEvent.SwapHand) {
            onSwapHands(player);
            return;
        }

        for (SlotTransaction transaction : event.getTransactions()) {
            int index = transaction.getSlot().getInventoryProperty(SlotIndex.class).get().getValue();
            boolean isOffHand = index == OFFHAND_SLOT_INDEX;
            if (!isOffHand && !isHotbarSlot(index)) {
                // I don't care, oh, oh
                continue;
            }

            if (ItemStackUtil.equalsIgnoringQuantity(transaction.getOriginal().createStack(), transaction.getFinal().createStack())) {
                continue;
            }

            HandType hand = isOffHand ? HandTypes.OFF_HAND : HandTypes.MAIN_HAND;
            // So we want to know, are they removing a previous wand from their hand
            if ((isOffHand || isSelected(player, index)) && isDesiredItem(transaction.getOriginal())) {
                onStopHolding(player, transaction.getOriginal(), hand);
            }

            if ((isOffHand || isSelected(player, index)) && isDesiredItem(transaction.getFinal())) {
                onStartHolding(player, transaction.getFinal(), hand);
            }
        }
    }

    @Listener(order = Order.POST)
    public void onDeath(DestructEntityEvent.Death event) {
        if (!(event.getTargetEntity() instanceof Player) || event.getKeepInventory()) {
            return;
        }
        Player player = (Player) event.getTargetEntity();
        stopHoldingAll(player);
    }

    @Listener
    public void onDisconnect(ClientConnectionEvent.Disconnect event) {
        stopHoldingAll(event.getTargetEntity());
    }

    @Listener
    public void onJoin(ClientConnectionEvent.Join event) {
        Player player = event.getTargetEntity();
        player.getItemInHand(HandTypes.MAIN_HAND)
                .map(ItemStack::createSnapshot)
                .filter(this::isDesiredItem)
                .ifPresent(item -> onStartHolding(player, item, HandTypes.MAIN_HAND));

        player.getItemInHand(HandTypes.OFF_HAND)
                .map(ItemStack::createSnapshot)
                .filter(this::isDesiredItem)
                .ifPresent(item -> onStartHolding(player, item, HandTypes.OFF_HAND));
    }

    private void stopHoldingAll(Player player) {
        player.getItemInHand(HandTypes.MAIN_HAND)
                .map(ItemStack::createSnapshot)
                .filter(this::isDesiredItem)
                .ifPresent(item -> onStopHolding(player, item, HandTypes.MAIN_HAND));

        player.getItemInHand(HandTypes.OFF_HAND)
                .map(ItemStack::createSnapshot)
                .filter(this::isDesiredItem)
                .ifPresent(item -> onStopHolding(player, item, HandTypes.OFF_HAND));
    }
}
