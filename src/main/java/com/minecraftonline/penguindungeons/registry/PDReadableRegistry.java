package com.minecraftonline.penguindungeons.registry;

import com.minecraftonline.penguindungeons.util.ResourceKey;

import java.util.Optional;

public interface PDReadableRegistry<T> {
    Optional<T> find(ResourceKey resourceKey);
}
