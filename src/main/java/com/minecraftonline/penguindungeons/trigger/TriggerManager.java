package com.minecraftonline.penguindungeons.trigger;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TriggerManager {

    private final Set<String> invalidDungeons = new HashSet<>(); // Only warn once per invalid dungeon.
    private final Map<String, Trigger> triggers = new HashMap<>();

    public void register(String id, Trigger trigger) {
        PenguinDungeons.getLogger().info("Trigger " + id + " registered");
        checkTrigger(id, trigger);
        trigger.register(() -> onTriggerFired(trigger));
        this.triggers.put(id, trigger);
    }

    private void checkTrigger(String id, Trigger trigger) {
        for (String dungeonName : trigger.getDungeonsToSpawn()) {
            if (!PenguinDungeons.getDungeons().containsKey(dungeonName)) {
                PenguinDungeons.getLogger().error("Trigger config '" + id + "' specifies dungeon '" + dungeonName + "' but that dungeon doesn't exist!");
                this.invalidDungeons.add(dungeonName);
            }
        }
    }

    private void onTriggerFired(final Trigger trigger) {
        PenguinDungeons.getLogger().info("Trigger fired!");
        PenguinDungeons.getLogger().info("Spawning " + trigger.getDungeonsToSpawn().size() + " dungeons");
        for (String dungeonName : trigger.getDungeonsToSpawn()) {
            PenguinDungeons.getLogger().info("Trying to spawn dungeon: " + dungeonName);
            Dungeon dungeon = PenguinDungeons.getDungeons().get(dungeonName);
            if (dungeon == null) {
                if (this.invalidDungeons.contains(dungeonName)) {
                    continue;
                }
                PenguinDungeons.getLogger().error("Trigger file specifies dungeon that doesn't exit '" + dungeonName + "'");
                this.invalidDungeons.add(dungeonName);
                continue;
            }
            dungeon.spawn(false);
        }
    }
}
