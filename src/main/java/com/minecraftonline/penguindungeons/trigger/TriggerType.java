package com.minecraftonline.penguindungeons.trigger;

import com.google.common.reflect.TypeToken;
import com.minecraftonline.penguindungeons.util.PDTypeTokens;

import javax.annotation.Nullable;

public enum TriggerType {

    SUMMON(PDTypeTokens.SUMMON_TRIGGER_TOKEN),

    ;

    private final TypeToken<? extends Trigger> typeToken;

    TriggerType(TypeToken<? extends Trigger> typeToken) {
        this.typeToken = typeToken;
    }

    @Nullable
    public TypeToken<? extends Trigger> typeToken() {
        return this.typeToken;
    }
}
