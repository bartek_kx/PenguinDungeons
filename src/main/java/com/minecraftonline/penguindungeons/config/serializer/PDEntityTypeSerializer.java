package com.minecraftonline.penguindungeons.config.serializer;

import com.google.common.reflect.TypeToken;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class PDEntityTypeSerializer implements TypeSerializer<PDEntityType> {
    @Override
    public @Nullable PDEntityType deserialize(@NonNull TypeToken<?> type, @NonNull ConfigurationNode value) throws ObjectMappingException {
        if (value.getString() == null) {
            return null;
        }
        final ResourceKey key;
        try {
            key = ResourceKey.resolve(value.getString());
        } catch (IllegalArgumentException e) {
            throw new ObjectMappingException("Invalid ResourceKey string '" + value.getString() + "'", e);
        }
        return PenguinDungeons.getInstance().getPDEntityRegistry().find(key)
                .orElseThrow(() -> new ObjectMappingException("Cannot find entity with id '" + key + "'"));
    }

    @Override
    public void serialize(@NonNull TypeToken<?> type, @Nullable PDEntityType obj, @NonNull ConfigurationNode value) throws ObjectMappingException {
        if (obj == null) {
            value.setValue(null);
            return;
        }
        value.setValue(obj.getId().asString());
    }
}
