package com.minecraftonline.penguindungeons.config.serializer;

import com.google.common.reflect.TypeToken;
import com.minecraftonline.penguindungeons.customentity.NBTEntityType;
import com.minecraftonline.penguindungeons.customentity.FireworkHelper;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.persistence.DataFormats;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;

import java.io.IOException;
import java.util.Optional;

public class NBTEntityTypeSerializer implements TypeSerializer<NBTEntityType> {

    private static final String ENTITY_TYPE_NODE = "entity-type";
    private static final String NBT_NODE = "nbt";

    private static final DataQuery FireworksItemTag = DataQuery.of("EntityTag", "FireworksItem", "tag");

    @Override
    public @Nullable NBTEntityType deserialize(@NonNull TypeToken<?> type, @NonNull ConfigurationNode value) throws ObjectMappingException {
        final String key = value.getKey().toString();
        final String entityType = value.getNode(ENTITY_TYPE_NODE).getString();
        if (entityType == null) {
            throw new ObjectMappingException("Missing entity type in custom entity!");
        }
        final EntityType entity = Sponge.getRegistry().getType(EntityType.class, entityType)
                .orElseThrow(() -> new ObjectMappingException("Unknown entity type: '" + entityType + "'"));
        final String nbt = value.getNode(NBT_NODE).getString();
        if (nbt == null) {
            throw new ObjectMappingException("Missing nbt in custom entity!");
        }
        final DataContainer json;
        try {
            json = DataFormats.JSON.read(nbt);
        } catch (IOException e) {
            throw new ObjectMappingException("Invalid json", e);
        }

        if (entity == EntityTypes.FIREWORK) {
            Optional<DataView> itemTag = json.getView(FireworksItemTag);
            // make sure fireworks use int arrays (instead of a list of ints) for firework colors
            if (itemTag.isPresent()) {
                json.set(FireworksItemTag, FireworkHelper.convertIntListsToIntArrays(itemTag.get()));
            }
        }

        return new NBTEntityType(ResourceKey.pdConfig(key), entity, json);
    }

    @Override
    public void serialize(@NonNull TypeToken<?> type, @Nullable NBTEntityType obj, @NonNull ConfigurationNode value) throws ObjectMappingException {
        if (obj == null) {
            value.setValue(null);
            return;
        }
        value.getNode(ENTITY_TYPE_NODE).setValue(obj.getEntityType().getId());
        final String json;
        try {
            json = DataFormats.JSON.write(obj.getNbt());
        } catch (IOException e) {
            throw new ObjectMappingException("Invalid json", e);
        }
        value.getNode(NBT_NODE).setValue(json);
    }
}
