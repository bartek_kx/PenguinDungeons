package com.minecraftonline.penguindungeons.command.commands.test;

import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.command.AbstractCommand;
import com.minecraftonline.penguindungeons.gui.sign.SignGui;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

import java.util.Arrays;
import java.util.List;

public class TestSignEditor extends AbstractCommand {

    public TestSignEditor() {
        setExecutor((src, args) -> {
            if (!(src instanceof Player)) {
                throw new CommandException(Text.of("You must be a player to use this command!"));
            }
            final Player player = (Player) src;
            List<Text> startingLines = Arrays.asList(Text.of("Feel free to edit"), Text.of("this as you"), Text.of("please!"), Text.of());
            SignGui signGui = new SignGui(startingLines, (ply, lines) -> {
                ply.sendMessage(Text.of("You finished editing the sign!"));
                for (int i = 0; i < lines.length; i++) {
                    ply.sendMessage(Text.of("Line " + i + ": '" + lines[i] + "'"));
                }
            });
            PenguinDungeons.getInstance().getOpenSignEditorManager().openSignGui(player, signGui);
            return CommandResult.success();
        });
    }
}
