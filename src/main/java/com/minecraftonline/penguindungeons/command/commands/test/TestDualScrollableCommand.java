package com.minecraftonline.penguindungeons.command.commands.test;

import com.minecraftonline.penguindungeons.command.AbstractCommand;
import com.minecraftonline.penguindungeons.gui.inventory.DualScrollableInventoryList;
import com.minecraftonline.penguindungeons.gui.inventory.ScrollableInventoryListInfo;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.ArrayList;
import java.util.List;

public class TestDualScrollableCommand extends AbstractCommand {

    public TestDualScrollableCommand() {
        setExecutor((src, args) -> {
            if (!(src instanceof Player)) {
                throw new CommandException(Text.of("You must be a player to execute this command!"));
            }
            final Player player = (Player) src;

            List<ItemStack> items = new ArrayList<>();
            List<ItemStack> blocks = new ArrayList<>();

            Sponge.getRegistry().getAllOf(ItemType.class).forEach(itemType -> {
                if (itemType.getBlock().isPresent()) {
                    blocks.add(ItemStack.of(itemType));
                } else {
                    items.add(ItemStack.of(itemType));
                }
            });

            ScrollableInventoryListInfo itemsInfo = new ScrollableInventoryListInfo(
                    Text.of(TextColors.DARK_GRAY, "Test Dual Scrollable Inventory 1 of 2 - Items"),
                    items,
                    ((p, itemStack, index, shift) -> {
                        p.sendMessage(Text.of("Inventory: Items"));
                        p.sendMessage(Text.of("Item: " + itemStack));
                        p.sendMessage(Text.of("Index: " + index));
                        p.sendMessage(Text.of("Shifting: " + shift));
                    }));

            ScrollableInventoryListInfo blocksInfo = new ScrollableInventoryListInfo(
                    Text.of(TextColors.DARK_GRAY, "Test Dual Scrollable Inventory 2 of 2 - Blocks"),
                    blocks,
                    ((p, itemStack, index, shift) -> {
                        p.sendMessage(Text.of("Inventory: Blocks"));
                        p.sendMessage(Text.of("Item: " + itemStack));
                        p.sendMessage(Text.of("Index: " + index));
                        p.sendMessage(Text.of("Shifting: " + shift));
                    }));

            ItemStack switchToBlocks = ItemStack.builder()
                    .itemType(ItemTypes.GOLD_BLOCK)
                    .add(Keys.DISPLAY_NAME, Text.of(TextColors.WHITE, "Switch to blocks"))
                    .build();

            ItemStack switchToItems = ItemStack.builder()
                    .itemType(ItemTypes.GOLD_NUGGET)
                    .add(Keys.DISPLAY_NAME, Text.of(TextColors.WHITE, "Switch to items"))
                    .build();

            final DualScrollableInventoryList dualScrollableInventoryList = new DualScrollableInventoryList(itemsInfo, blocksInfo, switchToBlocks, switchToItems);
            player.openInventory(dualScrollableInventoryList.create());
            return CommandResult.success();
        });
    }
}
