package com.minecraftonline.penguindungeons.customentity.other;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.spongepowered.api.boss.BossBarColors;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.monster.WitherSkeleton;
import org.spongepowered.api.entity.living.monster.Vex;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.EntityAIHurtByNonPD;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarColorData;
import com.minecraftonline.penguindungeons.data.bossbar.PDBossBarData;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackRangedBow;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.monster.EntityWitherSkeleton;
import net.minecraft.entity.monster.EntityVex;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;

public class FlyingCupid extends AbstractCustomEntity {

    private static final double NO_DROP_CHANCE = 0.0;
    private static final double BOSS_HEALTH = 300.0;

    public FlyingCupid(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.WITHER_SKELETON;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.RED, "Cupid");
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Flying Cupid boss"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof WitherSkeleton)) {
            throw new IllegalArgumentException("Expected a WitherSkeleton to be given to FlyingCupid to load, but got: " + entity);
        }
        WitherSkeleton witherSkeleton = (WitherSkeleton) entity;

        // clear tasks (normally has avoiding sunlight/wolves)
        // no need to keep wandering/movement tasks as Vex controls movement
        Goal<Agent> normalGoals = witherSkeleton.getGoal(GoalTypes.NORMAL).get();
        normalGoals.clear();
        EntityWitherSkeleton mcWitherSkeleton = (EntityWitherSkeleton) witherSkeleton;
        normalGoals.addTask(6, (AITask<? extends Agent>) new EntityAIAttackRangedBow<EntityWitherSkeleton>(mcWitherSkeleton, 0.0D, 20, 15.0F));
        normalGoals.addTask(9, (AITask<? extends Agent>) new EntityAIWatchClosest(mcWitherSkeleton, EntityPlayer.class, 3.0F, 1.0F));
        normalGoals.addTask(10, (AITask<? extends Agent>) new EntityAIWatchClosest(mcWitherSkeleton, EntityPlayer.class, 8.0F));
        normalGoals.addTask(6, (AITask<? extends Agent>) new EntityAILookIdle(mcWitherSkeleton));

        Goal<Agent> targetGoals = witherSkeleton.getGoal(GoalTypes.TARGET).get();

        // replace target goals
        targetGoals.clear();
        targetGoals.addTask(1, new EntityAIHurtByNonPD.TargetNonPDAttackers((Creature) entity, true, true));
        targetGoals.addTask(2, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityPlayer>(mcWitherSkeleton, EntityPlayer.class, false, false));
        // intentionally attack villagers
        targetGoals.addTask(3, (AITask<? extends Agent>) new EntityAINearestAttackableTarget<EntityVillager>(mcWitherSkeleton, EntityVillager.class, false, false));
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        WitherSkeleton witherSkeleton = (WitherSkeleton) world.createEntity(getType(), blockPos);

        witherSkeleton.offer(new PDEntityTypeData(getId()));
        witherSkeleton.offer(new OwnerLootData(ownerLoot()));
        witherSkeleton.offer(new PDSpawnData(true));
        witherSkeleton.offer(new PDBossBarData(true));
        witherSkeleton.offer(new PDBossBarColorData(BossBarColors.RED));
        witherSkeleton.offer(Keys.DISPLAY_NAME, getDisplayName());

        // boss health
        witherSkeleton.offer(Keys.MAX_HEALTH, BOSS_HEALTH);
        witherSkeleton.offer(Keys.HEALTH, BOSS_HEALTH);

        witherSkeleton.offer(Keys.IS_SILENT, true);

        // entity should despawn
        witherSkeleton.offer(Keys.PERSISTS, false);

        applyEquipment(witherSkeleton);
        applyLootTable(witherSkeleton);

        // use PD love Vex
        Vex vex = CustomEntityTypes.LOVE_VEX.makeVex(witherSkeleton.getWorld(), blockPos);
        EntityVex mcVex = ((EntityVex) vex);

        vex.offer(Keys.PERSISTS, false);
        vex.offer(Keys.IS_SILENT, true);
        mcVex.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(0.0D);
        mcVex.addPotionEffect(new PotionEffect(MobEffects.INVISIBILITY, Integer.MAX_VALUE, 0, false, false));
        mcVex.setOwner((EntityWitherSkeleton) witherSkeleton);
        mcVex.getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE).applyModifier(new AttributeModifier("Cupid", 30, 0));

        return world1 -> {
            world1.spawnEntity(witherSkeleton);
            onLoad(witherSkeleton);
            world1.spawnEntity(vex);
            vex.addPassenger(witherSkeleton);
            CustomEntityTypes.LOVE_VEX.onLoad(vex);
            return vex;
        };
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {

        List<Enchantment> enchants = new ArrayList<Enchantment>();
        enchants.add(Enchantment.of(EnchantmentTypes.POWER, 3));
        enchants.add(Enchantment.of(EnchantmentTypes.MENDING, 1));
        enchants.add(Enchantment.of(EnchantmentTypes.UNBREAKING, 3));
        // vanishing so this item does not drop, only the loot table one
        enchants.add(Enchantment.of(EnchantmentTypes.VANISHING_CURSE, 1));
        // bow held by boss has extra punch
        enchants.add(Enchantment.of(EnchantmentTypes.PUNCH, 2));

        final ItemStack bow = ItemStack.builder()
                .itemType(ItemTypes.BOW)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.DARK_PURPLE, "Bow d'\u00E9namoration"))
                .add(Keys.ITEM_LORE, Arrays.asList(
                    Text.of(TextColors.GRAY, "An infatuating bow that strikes those it hits with feelings of love"),
                    Text.of(TextColors.GRAY, "Feels like being shot by any other bow!"),
                    Text.of(TextColors.DARK_PURPLE, "[\u0393]")
                    ))
                .add(Keys.ITEM_ENCHANTMENTS, enchants)
                .build();
        return super.makeEquipmentLoadout()
                .mainHand(bow, NO_DROP_CHANCE);
    }

}
