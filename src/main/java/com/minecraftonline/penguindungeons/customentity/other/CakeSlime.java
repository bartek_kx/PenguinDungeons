package com.minecraftonline.penguindungeons.customentity.other;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.Slime;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.ai.EntityAIFindEntityNearestPlayer;
import net.minecraft.entity.monster.EntitySlime;

public class CakeSlime extends AbstractCustomEntity {

    public static final double NEW_HEALTH = 15.0;

    public CakeSlime(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.SLIME;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.WHITE, "Cake");
    }

    @Override
    public ItemStack getSpawnEgg() {
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG)
                .add(Keys.SPAWNABLE_ENTITY_TYPE, getType())
                .add(Keys.DISPLAY_NAME, getDisplayName())
                .add(Keys.ITEM_LORE, getDescription())
                .itemData(new PDEntityTypeData(getId()))
                .build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return Spawnable.of(makeSlime(world, pos), this::onLoad);
    }

    protected Slime makeSlime(World world, Vector3d blockPos) {

        Slime slime = (Slime) world.createEntity(getType(), blockPos);
        slime.offer(new PDEntityTypeData(getId()));
        slime.offer(new OwnerLootData(ownerLoot()));
        slime.offer(new PDSpawnData(true));
        slime.offer(Keys.DISPLAY_NAME, getDisplayName());
        slime.offer(Keys.PERSISTS, false);
        slime.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        slime.offer(Keys.HEALTH, NEW_HEALTH);
        // smallest size slime, so it does not split in to default (but same name) slimes
        slime.offer(Keys.SLIME_SIZE, 0);
        slime.offer(Keys.IS_SILENT, true);
        applyEquipment(slime);
        applyLootTable(slime);
        return slime;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity)
    {
        if (!(entity instanceof Slime)) {
            throw new IllegalArgumentException("Expected a Slime to be given to CakeSlime to load, but got: " + entity);
        }
        Slime slime = (Slime) entity;
        EntitySlime entitySlime = (EntitySlime) slime;

        Goal<Agent> targetGoals = slime.getGoal(GoalTypes.TARGET).get();
        targetGoals.clear();
        // slimes have no path finding or sight checking, so just find the nearest player
        targetGoals.addTask(1, (AITask<? extends Agent>) new EntityAIFindEntityNearestPlayer(entitySlime));

        // health has to be reset here because vanilla loading from nbt sets max health
        slime.offer(Keys.MAX_HEALTH, NEW_HEALTH);
        slime.offer(Keys.HEALTH, NEW_HEALTH);
    }

    public boolean ownerLoot()
    {
        return true;
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A cake monster"));
    }

}
