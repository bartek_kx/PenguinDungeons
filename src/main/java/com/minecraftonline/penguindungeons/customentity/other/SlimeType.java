package com.minecraftonline.penguindungeons.customentity.other;

import org.spongepowered.api.entity.living.monster.Slime;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.CustomEntityType;

public interface SlimeType extends CustomEntityType {

    Slime makeSlime(World world, Vector3d blockPos, int size);

}
