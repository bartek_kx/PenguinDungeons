package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.animal.ZombieHorse;
import org.spongepowered.api.entity.living.monster.Zombie;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.loot.OwnerLootData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.passive.EntityZombieHorse;

public class HorseZombie extends ZombieType<Zombie> {

    public HorseZombie(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.ZOMBIE;
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.DARK_GREEN, "Zombie Horseman");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A zombie riding a zombie horse"));
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Zombie zombie = (Zombie) world.createEntity(getType(), blockPos);
        // don't set custom name
        zombie.offer(new PDEntityTypeData(getId()));
        zombie.offer(new OwnerLootData(ownerLoot()));
        zombie.offer(new PDSpawnData(true));

        // entity should despawn
        zombie.offer(Keys.PERSISTS, false);

        applyEquipment(zombie);
        applyLootTable(zombie);

        ZombieHorse zombieHorse = (ZombieHorse) world.createEntity(EntityTypes.ZOMBIE_HORSE, blockPos);

        EntityZombieHorse entityZombieHorse = (EntityZombieHorse) zombieHorse;
        entityZombieHorse.setHorseTamed(true);
        entityZombieHorse.setGrowingAge(0);

        return world1 -> {
            world1.spawnEntity(zombie);
            onLoad(zombie);
            world1.spawnEntity(zombieHorse);
            zombieHorse.addPassenger(zombie);
            return zombieHorse;
        };
    }

}
