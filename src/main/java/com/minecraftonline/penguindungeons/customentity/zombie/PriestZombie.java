package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.Zombie;
import org.spongepowered.api.entity.living.monster.ZombieVillager;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.customentity.ProjectileData;
import com.minecraftonline.penguindungeons.customentity.StraightProjectileAttack;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.ai.EntityAIZombieAttack;
import net.minecraft.entity.monster.EntityZombieVillager;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumParticleTypes;

public class PriestZombie extends ZombieType<ZombieVillager> {

    public PriestZombie(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.ZOMBIE_VILLAGER;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.DARK_PURPLE, "Necropriest");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A zombie that has an area of effect attack which hurts players and heals zombies"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity) {
        super.onLoad(entity);
        if (!(entity instanceof ZombieVillager)) {
            throw new IllegalArgumentException("Expected a ZombieVillager to be given to PriestZombie to load, but got: " + entity);
        }
        Zombie zombie = (Zombie) entity;

        Goal<Agent> normalGoals = zombie.getGoal(GoalTypes.NORMAL).get();
        // Remove regular attack.
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIZombieAttack)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        normalGoals.addTask(1, new StraightProjectileAttack.RangedAttack(zombie,
                Optional.of(Text.of(TextColors.DARK_PURPLE, "Magic Skull")),
                AIUtil.ProjectileCreator.forSkull(AIUtil.ProjectileForImpact.forImpactWithSound(
                        AIUtil.ProjectileForImpact.forImpactAreaOfEffect(0.5F, TICKS_PER_SECOND*3,
                                new ProjectileData((net.minecraft.potion.PotionEffect)
                                        PotionEffect.of(PotionEffectTypes.INSTANT_DAMAGE, 0, 1),
                                        EnumParticleTypes.SPELL_WITCH, 0, 0)),
                        SoundEvents.BLOCK_ENCHANTMENT_TABLE_USE)), 1.0D, 30, 60, 15F));
    }

    @Override
    protected ZombieVillager makeZombie(World world, Vector3d blockPos) {
        ZombieVillager zombie = super.makeZombie(world, blockPos);
        EntityZombieVillager mcZombie = (EntityZombieVillager) zombie;
        mcZombie.setProfession(2); // purple robe
        return zombie;
    }
}
