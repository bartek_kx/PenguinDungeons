package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.monster.Zombie;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class PoliceZombie extends ZombieType<Zombie> {

    public PoliceZombie(ResourceKey key) {
        super(key);
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.DARK_GREEN, "Officer Zombie");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "Spawns a police zombie"));
    }

    @Override
    public Zombie makeZombie(World world, Vector3d blockPos) {
        Zombie zombie = super.makeZombie(world, blockPos);
        zombie.offer(Keys.MAX_HEALTH, 25.0D);
        zombie.offer(Keys.HEALTH, 25.0D);
        return zombie;
    }

}
