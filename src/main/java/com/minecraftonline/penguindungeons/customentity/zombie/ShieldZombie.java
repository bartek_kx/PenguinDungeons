package com.minecraftonline.penguindungeons.customentity.zombie;

import java.util.Collections;
import java.util.List;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.monster.Zombie;
import org.spongepowered.api.entity.living.monster.ZombieVillager;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.customentity.ShieldAttack;
import com.minecraftonline.penguindungeons.equipment.EquipmentLoadout;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.ai.EntityAIZombieAttack;
import net.minecraft.entity.monster.EntityZombieVillager;
import net.minecraft.util.EnumHand;

public class ShieldZombie extends ZombieType<ZombieVillager> {

    private static final double NO_DROP_CHANCE = 0.0;

    public ShieldZombie(ResourceKey key) {
        super(key);
    }

    @Override
    public EntityType getType() {
        return EntityTypes.ZOMBIE_VILLAGER;
    }

    public Text getDisplayName() {
        return Text.of(TextColors.GRAY, "Knight Zombie");
    }

    @Override
    public List<Text> getEntityDescription() {
        return Collections.singletonList(Text.of(TextColors.WHITE, "A zombie with a shield"));
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onLoad(Entity entity) {
        super.onLoad(entity);
        if (!(entity instanceof ZombieVillager)) {
            throw new IllegalArgumentException("Expected a ZombieVillager to be given to ShieldZombie to load, but got: " + entity);
        }
        Zombie zombie = (Zombie) entity;

        Goal<Agent> normalGoals = zombie.getGoal(GoalTypes.NORMAL).get();
        // Remove regular attack.
        normalGoals.getTasks().stream().filter(aiTask -> aiTask instanceof EntityAIZombieAttack)
                .forEach(task -> normalGoals.removeTask((AITask<? extends Agent>) task));

        normalGoals.addTask(1, new ShieldAttack.AttackWithShield(zombie, 0.8D, false, EnumHand.OFF_HAND));
    }

    @Override
    protected ZombieVillager makeZombie(World world, Vector3d blockPos) {
        ZombieVillager zombie = super.makeZombie(world, blockPos);
        EntityZombieVillager mcZombie = (EntityZombieVillager) zombie;
        mcZombie.setProfession(3); // blacksmith
        return zombie;
    }

    @Override
    public EquipmentLoadout.Builder makeEquipmentLoadout() {
        final ItemStack helmet = ItemStack.builder()
                .itemType(ItemTypes.IRON_HELMET)
                .build();
        final ItemStack chestplate = ItemStack.builder()
                .itemType(ItemTypes.IRON_CHESTPLATE)
                .build();
        final ItemStack shield = ItemStack.builder()
                .itemType(ItemTypes.SHIELD)
                .build();
        final ItemStack sword = ItemStack.builder()
                .itemType(ItemTypes.IRON_SWORD)
                .build();
        return super.makeEquipmentLoadout()
                .head(helmet, NO_DROP_CHANCE)
                .chest(chestplate, NO_DROP_CHANCE)
                .mainHand(sword, NO_DROP_CHANCE)
                .offHand(shield, NO_DROP_CHANCE);
    }
}
