package com.minecraftonline.penguindungeons.customentity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.asset.Asset;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.Queries;
import org.spongepowered.api.data.persistence.DataFormats;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.world.World;
import org.spongepowered.common.util.Constants;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.PenguinDungeons;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.data.customentity.PDEntityTypeData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnData;
import com.minecraftonline.penguindungeons.data.spawning.PDSpawnDataBuilder;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class StoredNBTEntityType implements CustomEntityType {

    private static final DataQuery ENTITY_ID_QUERY = DataQuery.of("EntityTag", "id");

    private NBTEntityType nbtEntity = null;
    private final ResourceKey resourceKey;
    private final String assetName;

    public StoredNBTEntityType(ResourceKey resourceKey, String assetName) {
        this.resourceKey = resourceKey;
        this.assetName = assetName;
    }

    @Override
    public final ResourceKey getId() {
        return this.resourceKey;
    }

    public void load(final PluginContainer pluginContainer) throws IOException {
        final Logger log = PenguinDungeons.getLogger();
        Optional<Asset> asset = pluginContainer.getAsset(assetName);
        if (!asset.isPresent()) {
            log.error("Failed to locate asset: '" + assetName + "', expected something");
            return;
        }

        final String json = asset.get().readString();
        final DataContainer eggNbt = DataFormats.JSON.read(json);

        if(!eggNbt.contains(ENTITY_ID_QUERY)) {
            log.error("Failed to load nbt entity asset: '" + assetName + "', expected an entity id, found none");
            return;
        }

        final String entityType = eggNbt.getString(ENTITY_ID_QUERY).get();
        final Optional<EntityType> entity = Sponge.getRegistry().getType(EntityType.class, entityType);
        if (!entity.isPresent()) {
            log.error("Failed to load nbt entity asset: '" + assetName + ", unknown entity type: '" + entityType + "'");
        }

        // apply PD entity type data
        // so that this entity can be tracked with a custom entity id
        List<DataView> manipulators = new ArrayList<DataView>();
        DataView pdEntityType = DataContainer.createNew();
        pdEntityType.set(Queries.CONTENT_VERSION, 2);
        pdEntityType.set(DataQuery.of(Constants.Sponge.MANIPULATOR_ID), PenguinDungeonKeys.PD_ENTITY_TYPE.getId());
        pdEntityType.set(Constants.Sponge.INTERNAL_DATA, new PDEntityTypeData(getId()).toContainer());
        manipulators.add(pdEntityType);
        // pdSpawn data is needed to tell that this entity was spawned by this plugin
        // with all the data already set (no need to replace entity)
        DataView pdSpawn = DataContainer.createNew();
        pdSpawn.set(Queries.CONTENT_VERSION, 2);
        pdSpawn.set(DataQuery.of(Constants.Sponge.MANIPULATOR_ID), PenguinDungeonKeys.PD_SPAWN.getId());
        pdSpawn.set(Constants.Sponge.INTERNAL_DATA, new PDSpawnData(true).toContainer());
        manipulators.add(pdSpawn);
        eggNbt.set(DataQuery.of("EntityTag", Constants.Forge.FORGE_DATA, Constants.Sponge.SPONGE_DATA,
                   Constants.Sponge.CUSTOM_MANIPULATOR_TAG_LIST), manipulators);

        this.nbtEntity = new NBTEntityType(getId(), entity.get(), eggNbt);
        log.info("Loaded asset '" + assetName + "', expected this to happen.");
    }

    @Override
    public EntityType getType() {
        if (this.nbtEntity != null) {
            return this.nbtEntity.getType();
        }
        return EntityTypes.UNKNOWN;
    }

    @Override
    public ItemStack getSpawnEgg() {
        if (this.nbtEntity != null) {
            return this.nbtEntity.getSpawnEgg();
        }
        return ItemStack.builder().itemType(ItemTypes.SPAWN_EGG).build();
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        if (this.nbtEntity != null) {
            return this.nbtEntity.createEntity(world, pos);
        }
        return Spawnable.empty();
    }

}
