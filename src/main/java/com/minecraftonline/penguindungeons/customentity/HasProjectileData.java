package com.minecraftonline.penguindungeons.customentity;

import net.minecraft.util.EnumParticleTypes;

public interface HasProjectileData extends HasEffects {

    public EnumParticleTypes getParticle();

    public int getParticleParam1();

    public int getParticleParam2();
}
