package com.minecraftonline.penguindungeons.customentity.shulker.standard;

import com.flowpowered.math.vector.Vector3d;
import com.google.common.collect.Lists;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.ai.Goal;
import org.spongepowered.api.entity.ai.GoalTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.ai.task.AbstractAITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.golem.Shulker;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class RainbowShulker extends StandardShulkerType {

    public RainbowShulker(ResourceKey key) {
        super(key);
    }

    private static final List<DyeColor> COLOURS = Lists.newArrayList(
            DyeColors.WHITE, DyeColors.ORANGE, DyeColors.MAGENTA, DyeColors.LIGHT_BLUE, DyeColors.YELLOW, DyeColors.LIME, DyeColors.PINK, DyeColors.GRAY,
            DyeColors.SILVER, DyeColors.CYAN, DyeColors.BLUE, DyeColors.PURPLE, DyeColors.GREEN, DyeColors.BROWN, DyeColors.RED, DyeColors.BLACK);

    @Override
    public List<Text> getEntityDescription() {
        return Arrays.asList(Text.of(TextColors.WHITE, "A shulker that changes colour"));
    }

    @Override
    public DyeColor getColor() {
        return COLOURS.get(0);
    }

    @Override
    public Text getDisplayName() {
        return Text.of(TextColors.DARK_RED, "R", TextColors.GOLD, "a", TextColors.YELLOW, "i", TextColors.GREEN, "n", TextColors.BLUE, "b",
                TextColors.DARK_BLUE, "o", TextColors.DARK_PURPLE, "w", TextColors.WHITE, " Shulker");
    }

    @Override
    public Spawnable createEntity(World world, Vector3d blockPos) {
        Entity shulker = super.makeShulker(world, blockPos);
        onLoad(shulker);
        return Spawnable.of(shulker);
    }

    @Override
    public void onLoad(Entity entity) {
        super.onLoad(entity);
        if (!(entity instanceof Shulker)) {
            throw new IllegalArgumentException("Expected a shulker to be given to Shulker to load, but got: " + entity);
        }
        Shulker shulker = (Shulker) entity;

        Goal<Agent> goal = shulker.getGoal(GoalTypes.NORMAL).get();

        goal.addTask(3, new ChangeColour());
    }

    public static class ChangeColour extends AbstractAITask<Shulker> {

        private int colourId;
        private int time;
        private static int SWAP_COLOR_INTERVAL = 5;

        public ChangeColour() {
            super(PenguinDungeonAITaskTypes.CHANGE_COLOUR);
        }

        @Override
        public void start() {
            Random rand = new Random();
            colourId = rand.nextInt(RainbowShulker.COLOURS.size());
            getOwner().get().offer(Keys.DYE_COLOR, RainbowShulker.COLOURS.get(colourId));
            time = 0;
        }

        @Override
        public boolean shouldUpdate() {
            if (!getOwner().isPresent()) {
                return false;
            }
            Shulker shulker = getOwner().get();
            return !shulker.isRemoved();
        }

        @Override
        public void update() {
            time++;
            if (time % SWAP_COLOR_INTERVAL == 0) {
              colourId++;
              colourId = colourId % RainbowShulker.COLOURS.size();
              getOwner().get().offer(Keys.DYE_COLOR, RainbowShulker.COLOURS.get(colourId));
            }
            if (time > SWAP_COLOR_INTERVAL) {
              time = 0;
            }
        }

        @Override
        public boolean continueUpdating() {
            return shouldUpdate();
        }

        @Override
        public void reset() {
            getOwner().get().offer(Keys.DYE_COLOR, RainbowShulker.COLOURS.get(colourId));
        }

        @Override
        public boolean canRunConcurrentWith(AITask<Shulker> other) {
            return true;
        }

        @Override
        public boolean canBeInterrupted() {
            return false;
        }
    }
}
