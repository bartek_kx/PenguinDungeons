package com.minecraftonline.penguindungeons.customentity.shulker.potion;

import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectType;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.text.Text;

import java.util.List;

public class SimplePotionEffectShulker extends PotionEffectShulker {

    private final Text displayName;
    private final List<Text> description;
    private final DyeColor dyeColor;
    private final PotionEffectType effect;
    private final int durationTicks;
    private final int amplifier;
    private final String bulletName;

    public SimplePotionEffectShulker(ResourceKey key, Text displayName, List<Text> description, DyeColor dyeColor,
                                     PotionEffectType effect, int durationTicks, int amplifier, String bulletName) {
        super(key, PenguinDungeonAITaskTypes.SHULKER_EFFECT_ATTACK);
        this.displayName = displayName;
        this.description = description;
        this.dyeColor = dyeColor;
        this.effect = effect;
        this.durationTicks = durationTicks;
        this.amplifier = amplifier;
        this.bulletName = bulletName;
    }

    public static Builder builder() {
        return new SimplePotionEffectShulkerBuilder();
    }

    @Override
    public EntityType getType() {
        return EntityTypes.SHULKER;
    }

    @Override
    public List<Text> getEntityDescription() {
        return this.description;
    }

    @Override
    public DyeColor getColor() {
        return this.dyeColor;
    }

    @Override
    public Text getDisplayName() {
        return this.displayName;
    }

    @Override
    protected PotionEffect getPotionEffect() {
        return PotionEffect.of(this.effect, this.amplifier, this.durationTicks);
    }

    @Override
    protected String getBulletName() {
        return this.bulletName;
    }

    public interface Builder {

        Builder key(final ResourceKey key);

        Builder displayName(final Text displayName);

        Builder description(final List<Text> description);

        Builder color(DyeColor dyeColor);

        Builder effect(PotionEffectType effect);

        Builder amplifier(int amplifier);

        Builder duration(int ticks);

        Builder bulletName(String name);

        SimplePotionEffectShulker build();
    }
}
