package com.minecraftonline.penguindungeons.customentity;

import java.util.Optional;

import javax.annotation.Nullable;

import org.spongepowered.api.entity.living.Creature;
import org.spongepowered.api.entity.living.Living;

import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.util.math.AxisAlignedBB;

public class EntityAIHurtByNonPD extends EntityAIHurtByTarget {
   private final boolean anyPDType;

   public EntityAIHurtByNonPD(EntityCreature creatureIn, boolean entityCallsForHelpIn, Class<?>... excludedReinforcementTypes) {
      this(creatureIn, entityCallsForHelpIn, false, excludedReinforcementTypes);
   }

   public EntityAIHurtByNonPD(EntityCreature creatureIn, boolean entityCallsForHelpIn, boolean anyPDType, Class<?>... excludedReinforcementTypes) {
      super(creatureIn, entityCallsForHelpIn, excludedReinforcementTypes);
      this.anyPDType = anyPDType;
   }

   @Override
   protected void alertOthers() {
       double d0 = this.getTargetDistance();

       if (anyPDType)
       {
          // alert other PenguinDungeon mobs even if they are not the same entity type
          for(EntityLiving entityliving : this.taskOwner.world.getEntitiesWithinAABB(EntityLiving.class, (new AxisAlignedBB(this.taskOwner.posX, this.taskOwner.posY, this.taskOwner.posZ, this.taskOwner.posX + 1.0D, this.taskOwner.posY + 1.0D, this.taskOwner.posZ + 1.0D)).grow(d0, 10.0D, d0))) {
             if (this.taskOwner != entityliving && (entityliving.getAttackTarget() == null || !entityliving.getAttackTarget().isEntityAlive()) && !entityliving.isOnSameTeam(this.taskOwner.getRevengeTarget())) {

                Living living = (Living) entityliving;
                // skip if not a penguin dungeon entity, or non-dungeon humans
                Optional<ResourceKey> pdType = living.get(PenguinDungeonKeys.PD_ENTITY_TYPE);
                if (!pdType.isPresent() || pdType.get().equals(CustomEntityTypes.STEVE_POLICE.getId())     ||
                                           pdType.get().equals(CustomEntityTypes.STANDING_HUMAN.getId())   ||
                                           pdType.get().equals(CustomEntityTypes.WALKING_HUMAN.getId())    ||
                                           pdType.get().equals(CustomEntityTypes.AGGRESSIVE_HUMAN.getId()) ||
                                           pdType.get().equals(CustomEntityTypes.DEFENSIVE_HUMAN.getId())  ||
                                           pdType.get().equals(CustomEntityTypes.PROTECTIVE_HUMAN.getId()) ||
                                           pdType.get().equals(CustomEntityTypes.GUARDING_HUMAN.getId())   ||
                                           pdType.get().equals(CustomEntityTypes.PIGCHINKO_PLAYER.getId())) continue;

                entityliving.setAttackTarget(this.taskOwner.getRevengeTarget());
              }
           }
       }

       super.alertOthers();

    }

   @Override
   protected boolean isSuitableTarget(@Nullable EntityLivingBase target, boolean includeInvincibles)
   {
       // other PenguinDungeon mobs are not suitable targets
       if (((Living)target).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent()) return false;
       return super.isSuitableTarget(target, includeInvincibles);
   }

   public static class TargetNonPDAttackers extends DelegatingToMCAI<Creature> {

       public TargetNonPDAttackers(Creature creature, boolean entityCallsForHelpIn, Class<?>... excludedReinforcementTypes) {
           this(creature, entityCallsForHelpIn, false, excludedReinforcementTypes);
       }

       public TargetNonPDAttackers(Creature creature, boolean entityCallsForHelpIn, boolean anyPDType, Class<?>... excludedReinforcementTypes) {
           super(PenguinDungeonAITaskTypes.ATTACK_BACK_NON_PD,
                   new EntityAIHurtByNonPD((EntityCreature) creature, entityCallsForHelpIn, anyPDType, excludedReinforcementTypes));
       }
   }
}
