package com.minecraftonline.penguindungeons.customentity;

import java.util.List;

import net.minecraft.potion.PotionEffect;

public interface HasEffects {

    List<PotionEffect> getEffects();

}
