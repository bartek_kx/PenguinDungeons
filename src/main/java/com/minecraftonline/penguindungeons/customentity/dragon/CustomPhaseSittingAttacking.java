package com.minecraftonline.penguindungeons.customentity.dragon;

import net.minecraft.entity.boss.dragon.phase.PhaseSittingAttacking;

public class CustomPhaseSittingAttacking extends PhaseSittingAttacking implements CustomIPhase {
    private int attackingTicks;
    private CustomDragon dragon;

    public CustomPhaseSittingAttacking(CustomDragon dragonIn) {
        super(dragonIn);
        this.dragon = dragonIn;
    }

    /**
     * Gives the phase a chance to update its status.
     * Called by dragon's onLivingUpdate. Only used when !worldObj.isRemote.
     */
    public void doLocalUpdate() {
        if (this.attackingTicks++ >= 40) {
            this.dragon.getPhaseManager().setPhase(CustomPhaseList.SITTING_FLAMING);
        }

    }

    /**
     * Called when this phase is set to active
     */
    public void initPhase() {
        this.attackingTicks = 0;
    }

    @Override
    public CustomPhaseList<? extends CustomIPhase> getCustomType() {
        return CustomPhaseList.SITTING_ATTACKING;
    }

}
