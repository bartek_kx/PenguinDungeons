package com.minecraftonline.penguindungeons.customentity.dragon;

import net.minecraft.entity.boss.dragon.phase.IPhase;

public interface CustomIPhase extends IPhase {

   CustomPhaseList<? extends CustomIPhase> getCustomType();
}

