package com.minecraftonline.penguindungeons.customentity.dragon;

import net.minecraft.block.BlockColored;
import net.minecraft.block.BlockDirt;
import net.minecraft.block.BlockStainedGlassPane;
import net.minecraft.block.BlockStainedHardenedClay;
import net.minecraft.block.BlockStone;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumDyeColor;

public enum CrystalType
{
    NONE(null, Blocks.OBSIDIAN.getDefaultState(), Blocks.IRON_BARS.getDefaultState(), Blocks.BEDROCK.getDefaultState(), false),
    ICE("\u00A73Snowflake Crystal", Blocks.PACKED_ICE.getDefaultState(),
            Blocks.STAINED_GLASS_PANE.getDefaultState().withProperty(BlockStainedGlassPane.COLOR, EnumDyeColor.LIGHT_BLUE)),
    TERRAIN("\u00A76Terrain Crystal", Blocks.STONE.getDefaultState().withProperty(BlockStone.VARIANT, BlockStone.EnumType.GRANITE),
            Blocks.SPRUCE_FENCE.getDefaultState(), Blocks.DIRT.getDefaultState().withProperty(BlockDirt.VARIANT, BlockDirt.DirtType.PODZOL),
            true, Blocks.DIRT.getDefaultState()),
    METAL("\u00A77Metal Crystal", Blocks.STONE.getDefaultState().withProperty(BlockStone.VARIANT, BlockStone.EnumType.ANDESITE_SMOOTH),
            Blocks.STAINED_GLASS_PANE.getDefaultState().withProperty(BlockStainedGlassPane.COLOR, EnumDyeColor.RED),
            Blocks.CONCRETE.getDefaultState().withProperty(BlockColored.COLOR, EnumDyeColor.SILVER), false),
    COMPANION("Companion Cube", Blocks.STONE.getDefaultState().withProperty(BlockStone.VARIANT, BlockStone.EnumType.DIORITE_SMOOTH),
            Blocks.STAINED_GLASS_PANE.getDefaultState().withProperty(BlockStainedGlassPane.COLOR, EnumDyeColor.PINK),
            Blocks.CONCRETE.getDefaultState().withProperty(BlockColored.COLOR, EnumDyeColor.WHITE), false),
    CHOCOLATE("\u00A76Chocolate Crystal", Blocks.CONCRETE.getDefaultState().withProperty(BlockColored.COLOR, EnumDyeColor.BROWN),
            Blocks.DARK_OAK_FENCE.getDefaultState(),
            Blocks.STAINED_HARDENED_CLAY.getDefaultState().withProperty(BlockStainedHardenedClay.COLOR, EnumDyeColor.BROWN), false),
    SPOOKY("\u00A76Spooky Crystal", Blocks.NETHER_BRICK.getDefaultState(), Blocks.NETHER_BRICK_FENCE.getDefaultState(),
            Blocks.NETHERRACK.getDefaultState(), false, Blocks.RED_NETHER_BRICK.getDefaultState());

    private final String name;
    private final IBlockState baseMaterial;
    private final IBlockState cageMaterial;
    private final IBlockState crystalMaterial;
    private final boolean useCrystalMaterialAsTop;
    private final IBlockState middleMaterial;

    public static CrystalType fromName(String name)
    {
        if (name == null) return NONE;
        else if (name.equals("\u00A73Snowflake Crystal")) return ICE;
        else if (name.equals("\u00A76Terrain Crystal")) return TERRAIN;
        else if (name.equals("\u00A77Metal Crystal")) return METAL;
        else if (name.toLowerCase().matches(".*companion[_ ]?cube.*")) return COMPANION;
        else if (name.equals("\u00A76Chocolate Crystal")) return CHOCOLATE;
        else if (name.equals("\u00A76Spooky Crystal")) return SPOOKY;
        else return NONE;
    }

    CrystalType(String name, IBlockState baseMaterial, IBlockState cageMaterial) {
        this(name, baseMaterial, cageMaterial, baseMaterial, false);
    }

    CrystalType(String name, IBlockState baseMaterial, IBlockState cageMaterial, IBlockState crystalMaterial, boolean useCrystalMaterialAsTop) {
        this(name, baseMaterial, cageMaterial, crystalMaterial, useCrystalMaterialAsTop, baseMaterial);
    }

    CrystalType(String name, IBlockState baseMaterial, IBlockState cageMaterial, IBlockState crystalMaterial, boolean useCrystalMaterialAsTop,
            IBlockState middleMaterial) {
        this.name = name;
        this.baseMaterial = baseMaterial;
        this.cageMaterial = cageMaterial;
        this.crystalMaterial = crystalMaterial;
        this.useCrystalMaterialAsTop = useCrystalMaterialAsTop;
        this.middleMaterial = middleMaterial;
    }

    public String getName() {
        return this.name;
    }

    public IBlockState getBaseMaterial() {
        return this.baseMaterial;
    }

    public IBlockState getCageMaterial() {
        return this.cageMaterial;
    }

    public IBlockState getCrystalMaterial() {
        return this.crystalMaterial;
    }

    public IBlockState getTopMaterial() {
        if (useCrystalMaterialAsTop) return this.crystalMaterial;
        else return this.middleMaterial;
    }

    public IBlockState getMiddleMaterial() {
        return this.middleMaterial;
    }
}
