package com.minecraftonline.penguindungeons.customentity.dragon;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Nullable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityMultiPart;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.MultiPartEntityPart;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.item.EntityEnderCrystal;
import net.minecraft.entity.monster.EntityGuardian;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathHeap;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.potion.PotionEffect;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityEndGateway;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.BossInfo;
import net.minecraft.world.BossInfoServer;
import net.minecraft.world.World;
import net.minecraft.world.end.DragonFightManager;
import net.minecraft.world.gen.feature.WorldGenEndPodium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.ai.AIUtil.DirectShulkerBulletCreator;
import com.minecraftonline.penguindungeons.ai.AIUtil.ProjectileCreator;
import com.minecraftonline.penguindungeons.customentity.AbstractCustomEntity;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.HasProjectileData;
import com.minecraftonline.penguindungeons.customentity.LaserGuardian;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

public class CustomDragon extends EntityDragon implements IEntityMultiPart, IMob, HasProjectileData {
    private static final Logger LOGGER = LogManager.getLogger();
    /** The dragon phase manager */
    private final CustomPhaseManager phaseManager;
    private int sittingDamageReceived;
    /**
     * A series of points describing three circles. The first is low to the ground and described by the first 12 points;
     * the next 8 points describe a spiral upwards to the next, tighter circle of just 4 points. Generated by
     * initPathPoints.
     */
    private final PathPoint[] pathPoints = new PathPoint[24];
    /**
     * An array of bitmaps indicating, for each member of pathPoints, which other members should be considered
     * "neighboring" for the purpose of path-finding. The set bits indicate the indexes into pathPoints that should be
     * considered to be candidates for the next step in a path through the circles. Generated by initPathPoints.
     */
    private final int[] neighbors = new int[24];
    private final PathHeap pathFindQueue = new PathHeap();
    public BlockPos podium = WorldGenEndPodium.END_PODIUM_LOCATION;
    private List<PotionEffect> effects = Arrays.asList(new PotionEffect(MobEffects.INSTANT_DAMAGE));
    private SoundEvent sound = SoundEvents.ENTITY_ENDERDRAGON_SHOOT;
    private EnumParticleTypes particle = EnumParticleTypes.DRAGON_BREATH;
    private int particleParam1 = 0;
    private int particleParam2 = 0;
    private String fireballName = "Dragon Fireball";
    private String bulletName = "Dragon Bullet";
    private ProjectileCreator fireballCreator = null;
    private DirectShulkerBulletCreator bulletCreator = null;
    private AbstractCustomEntity minionEntity = null;
    private AbstractCustomEntity alternateForm = null;
    private LaserGuardian guardian;
    private final BossInfoServer bossInfo;
    private final DragonFightManager fightManager;
    // default attacks
    public Set<AttackType> attackTypes = new HashSet<>(Arrays.asList(AttackType.FIREBALL, AttackType.BREATH, AttackType.CHARGE));

    public enum AttackType {
        FIREBALL,
        LIGHTNING,
        BREATH,
        CHARGE,
        BULLETS,
        LASER,
        MINIONS,
        TELEPORT,
        CHANGE_FORM
    }

    public CustomDragon(World worldIn) {
         this(worldIn, BossInfo.Color.PURPLE, null);
    }

    public CustomDragon(World worldIn, BossInfo.Color bossBarColor, DragonFightManager fightManager) {
        super(worldIn);

        this.phaseManager = new CustomPhaseManager(this);
        this.bossInfo = (BossInfoServer)(new BossInfoServer(this.getDisplayName(), bossBarColor, BossInfo.Overlay.PROGRESS));
        this.fightManager = fightManager;
        if (this.fightManager != null)
        {
            this.bossInfo.setVisible(false);
        }
    }

    public void setPodium(BlockPos podium)
    {
        this.podium = podium;
    }

    public void setEffects(List<PotionEffect> effects)
    {
        this.effects = effects;
    }

    public List<PotionEffect> getEffects()
    {
        return effects;
    }

    public void setSound(SoundEvent sound)
    {
        this.sound = sound;
    }

    public SoundEvent getSound()
    {
        return sound;
    }

    public void setParticle(EnumParticleTypes particle)
    {
        setParticle(particle, 0, 0);
    }

    public void setParticle(EnumParticleTypes particle, int particleParam1, int particleParam2)
    {
        this.particle = particle;
        this.particleParam1 = particleParam1;
        this.particleParam2 = particleParam2;
    }

    public EnumParticleTypes getParticle()
    {
        return this.particle;
    }

    public int getParticleParam1()
    {
        return this.particleParam1;
    }

    public int getParticleParam2()
    {
        return this.particleParam2;
    }

    public void setFireballName(String fireballName)
    {
        this.fireballName = fireballName;
    }

    @SuppressWarnings("deprecation")
    public void setFireballName(Text fireballName)
    {
        this.fireballName = TextSerializers.LEGACY_FORMATTING_CODE.serialize(fireballName);
    }

    public String getFireballName()
    {
        return this.fireballName;
    }

    public void setBulletName(String bulletName)
    {
        this.bulletName = bulletName;
    }

    @SuppressWarnings("deprecation")
    public void setBulletName(Text bulletName)
    {
        this.bulletName = TextSerializers.LEGACY_FORMATTING_CODE.serialize(bulletName);
    }

    public String getBulletName()
    {
        return this.bulletName;
    }

    public void setFireballCreator(ProjectileCreator fireballCreator)
    {
        this.fireballCreator = fireballCreator;
    }

    public ProjectileCreator getFireballCreator()
    {
        return this.fireballCreator;
    }

    public void setBulletName(DirectShulkerBulletCreator bulletCreator)
    {
        this.bulletCreator = bulletCreator;
    }

    public DirectShulkerBulletCreator getBulletCreator()
    {
        return this.bulletCreator;
    }

    // creates default fireball and bullet projectiles from dragon effects/particles/sounds
    protected void createDefaultProjectiles()
    {
        this.fireballCreator = AIUtil.ProjectileCreator.forDragonFireball(
                AIUtil.ProjectileForImpact.forImpactAreaOfEffect(3.0F, 600, this));
        this.bulletCreator = AIUtil.DirectShulkerBulletCreator.forDirectBullet(
                AIUtil.ShulkerBulletCreator.forHit2(this.getEffects()), this.getSound());
    }

    public void setMinionEntityType(AbstractCustomEntity minionEntity)
    {
        this.minionEntity = minionEntity;
    }

    public AbstractCustomEntity getMinionEntityType()
    {
        return this.minionEntity;
    }

    public void setAlternateFormEntityType(AbstractCustomEntity minionEntity)
    {
        this.alternateForm = minionEntity;
    }

    public AbstractCustomEntity getAlternateFormEntityType()
    {
        return this.alternateForm;
    }

    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    @Override
    public void onLivingUpdate() {
        this.prevAnimTime = this.animTime;
        if (this.getHealth() <= 0.0F) {
            if (this.guardian != null) {
                // remove the laser
                this.guardian.setHealth(0.0f);
                this.world.removeEntity(this.guardian);
                this.guardian = null;
            }
            float f12 = (this.rand.nextFloat() - 0.5F) * 8.0F;
            float f13 = (this.rand.nextFloat() - 0.5F) * 4.0F;
            float f15 = (this.rand.nextFloat() - 0.5F) * 8.0F;
            this.world.spawnParticle(EnumParticleTypes.EXPLOSION_LARGE, this.posX + (double)f12, this.posY + 2.0D + (double)f13, this.posZ + (double)f15, 0.0D, 0.0D, 0.0D);
        } else {
            this.updateDragonEnderCrystal();
            float f11 = 0.2F / (MathHelper.sqrt(this.motionX * this.motionX + this.motionZ * this.motionZ) * 10.0F + 1.0F);
            f11 = f11 * (float)Math.pow(2.0D, this.motionY);
            if (this.phaseManager.getCurrentPhase().getIsStationary()) {
                this.animTime += 0.1F;
            } else if (this.slowed) {
                this.animTime += f11 * 0.5F;
            } else {
                this.animTime += f11;
            }

            this.rotationYaw = MathHelper.wrapDegrees(this.rotationYaw);
            if (this.isAIDisabled()) {
                this.animTime = 0.5F;
            } else {
                if (this.ringBufferIndex < 0) {
                    for(int i = 0; i < this.ringBuffer.length; ++i) {
                        this.ringBuffer[i][0] = (double)this.rotationYaw;
                        this.ringBuffer[i][1] = this.posY;
                    }
                }

                if (++this.ringBufferIndex == this.ringBuffer.length) {
                    this.ringBufferIndex = 0;
                }

                this.ringBuffer[this.ringBufferIndex][0] = (double)this.rotationYaw;
                this.ringBuffer[this.ringBufferIndex][1] = this.posY;

                CustomIPhase iphase = this.phaseManager.getCurrentPhase();
                iphase.doLocalUpdate();
                if (this.phaseManager.getCurrentPhase() != iphase) {
                    iphase = this.phaseManager.getCurrentPhase();
                    iphase.doLocalUpdate();
                }

                if (this.attackTypes.contains(AttackType.LASER)) {
                    // create laser turret (guardian)
                    if (this.guardian == null)
                    {
                        this.guardian = new LaserGuardian(this.world, this);
                        this.guardian.setPosition(this.posX, this.posY + 2, this.posZ);
                        this.guardian.addPotionEffect(new PotionEffect(MobEffects.INVISIBILITY, 999999, 0, false, false));
                        this.guardian.setSilent(true);
                        this.guardian.setNoGravity(true);
                        this.guardian.setCustomNameTag("\u00a7cLaser Turret");
                        this.guardian.enablePersistence();
                        this.world.spawnEntity((EntityGuardian) this.guardian);
                    }
                    else
                    {
                        this.guardian.setPosition(this.posX, this.posY + 2, this.posZ);
                    }
                }

                Vec3d vec3d = iphase.getTargetLocation();
                if (vec3d != null) {
                    double d6 = vec3d.x - this.posX;
                    double d7 = vec3d.y - this.posY;
                    double d8 = vec3d.z - this.posZ;
                    double d3 = d6 * d6 + d7 * d7 + d8 * d8;
                    float f5 = iphase.getMaxRiseOrFall();
                    d7 = MathHelper.clamp(d7 / (double)MathHelper.sqrt(d6 * d6 + d8 * d8), (double)(-f5), (double)f5);
                    this.motionY += d7 * 0.10000000149011612D;
                    this.rotationYaw = MathHelper.wrapDegrees(this.rotationYaw);
                    double d4 = MathHelper.clamp(MathHelper.wrapDegrees(180.0D - MathHelper.atan2(d6, d8) * 57.2957763671875D - (double)this.rotationYaw), -50.0D, 50.0D);
                    Vec3d vec3d1 = (new Vec3d(vec3d.x - this.posX, vec3d.y - this.posY, vec3d.z - this.posZ)).normalize();
                    Vec3d vec3d2 = (new Vec3d((double)MathHelper.sin(this.rotationYaw * 0.017453292F), this.motionY, (double)(-MathHelper.cos(this.rotationYaw * 0.017453292F)))).normalize();
                    float f7 = Math.max(((float)vec3d2.dotProduct(vec3d1) + 0.5F) / 1.5F, 0.0F);
                    this.randomYawVelocity *= 0.8F;
                    this.randomYawVelocity = (float)((double)this.randomYawVelocity + d4 * (double)iphase.getYawFactor());
                    this.rotationYaw += this.randomYawVelocity * 0.1F;
                    float f8 = (float)(2.0D / (d3 + 1.0D));
                    float f9 = 0.06F;
                    this.moveRelative(0.0F, 0.0F, -1.0F, 0.06F * (f7 * f8 + (1.0F - f8)));
                    if (this.slowed) {
                        this.move(MoverType.SELF, this.motionX * 0.800000011920929D, this.motionY * 0.800000011920929D, this.motionZ * 0.800000011920929D);
                    } else {
                        this.move(MoverType.SELF, this.motionX, this.motionY, this.motionZ);
                    }

                    Vec3d vec3d3 = (new Vec3d(this.motionX, this.motionY, this.motionZ)).normalize();
                    float f10 = ((float)vec3d3.dotProduct(vec3d2) + 1.0F) / 2.0F;
                    f10 = 0.8F + 0.15F * f10;
                    this.motionX *= (double)f10;
                    this.motionZ *= (double)f10;
                    this.motionY *= 0.9100000262260437D;
                }

                this.renderYawOffset = this.rotationYaw;
                this.dragonPartHead.width = 1.0F;
                this.dragonPartHead.height = 1.0F;
                this.dragonPartNeck.width = 3.0F;
                this.dragonPartNeck.height = 3.0F;
                this.dragonPartTail1.width = 2.0F;
                this.dragonPartTail1.height = 2.0F;
                this.dragonPartTail2.width = 2.0F;
                this.dragonPartTail2.height = 2.0F;
                this.dragonPartTail3.width = 2.0F;
                this.dragonPartTail3.height = 2.0F;
                this.dragonPartBody.height = 3.0F;
                this.dragonPartBody.width = 5.0F;
                this.dragonPartWing1.height = 2.0F;
                this.dragonPartWing1.width = 4.0F;
                this.dragonPartWing2.height = 3.0F;
                this.dragonPartWing2.width = 4.0F;
                Vec3d[] avec3d = new Vec3d[this.dragonPartArray.length];

                for(int j = 0; j < this.dragonPartArray.length; ++j) {
                    avec3d[j] = new Vec3d(this.dragonPartArray[j].posX, this.dragonPartArray[j].posY, this.dragonPartArray[j].posZ);
                }

                float f14 = (float)(this.getMovementOffsets(5, 1.0F)[1] - this.getMovementOffsets(10, 1.0F)[1]) * 10.0F * 0.017453292F;
                float f16 = MathHelper.cos(f14);
                float f2 = MathHelper.sin(f14);
                float f17 = this.rotationYaw * 0.017453292F;
                float f3 = MathHelper.sin(f17);
                float f18 = MathHelper.cos(f17);
                this.dragonPartBody.onUpdate();
                this.dragonPartBody.setLocationAndAngles(this.posX + (double)(f3 * 0.5F), this.posY, this.posZ - (double)(f18 * 0.5F), 0.0F, 0.0F);
                this.dragonPartWing1.onUpdate();
                this.dragonPartWing1.setLocationAndAngles(this.posX + (double)(f18 * 4.5F), this.posY + 2.0D, this.posZ + (double)(f3 * 4.5F), 0.0F, 0.0F);
                this.dragonPartWing2.onUpdate();
                this.dragonPartWing2.setLocationAndAngles(this.posX - (double)(f18 * 4.5F), this.posY + 2.0D, this.posZ - (double)(f3 * 4.5F), 0.0F, 0.0F);
                if (!this.world.isRemote && this.hurtTime == 0) {
                    this.collideWithEntities(this.world.getEntitiesWithinAABBExcludingEntity(this, this.dragonPartWing1.getEntityBoundingBox().grow(4.0D, 2.0D, 4.0D).offset(0.0D, -2.0D, 0.0D)));
                    this.collideWithEntities(this.world.getEntitiesWithinAABBExcludingEntity(this, this.dragonPartWing2.getEntityBoundingBox().grow(4.0D, 2.0D, 4.0D).offset(0.0D, -2.0D, 0.0D)));
                    this.attackEntitiesInList(this.world.getEntitiesWithinAABBExcludingEntity(this, this.dragonPartHead.getEntityBoundingBox().grow(1.0D)));
                    this.attackEntitiesInList(this.world.getEntitiesWithinAABBExcludingEntity(this, this.dragonPartNeck.getEntityBoundingBox().grow(1.0D)));
                }

                double[] adouble = this.getMovementOffsets(5, 1.0F);
                float f19 = MathHelper.sin(this.rotationYaw * 0.017453292F - this.randomYawVelocity * 0.01F);
                float f4 = MathHelper.cos(this.rotationYaw * 0.017453292F - this.randomYawVelocity * 0.01F);
                this.dragonPartHead.onUpdate();
                this.dragonPartNeck.onUpdate();
                float f20 = this.getHeadYOffset(1.0F);
                this.dragonPartHead.setLocationAndAngles(this.posX + (double)(f19 * 6.5F * f16), this.posY + (double)f20 + (double)(f2 * 6.5F), this.posZ - (double)(f4 * 6.5F * f16), 0.0F, 0.0F);
                this.dragonPartNeck.setLocationAndAngles(this.posX + (double)(f19 * 5.5F * f16), this.posY + (double)f20 + (double)(f2 * 5.5F), this.posZ - (double)(f4 * 5.5F * f16), 0.0F, 0.0F);

                for(int k = 0; k < 3; ++k) {
                    MultiPartEntityPart multipartentitypart = null;
                    if (k == 0) {
                        multipartentitypart = this.dragonPartTail1;
                    }

                    if (k == 1) {
                        multipartentitypart = this.dragonPartTail2;
                    }

                    if (k == 2) {
                        multipartentitypart = this.dragonPartTail3;
                    }

                    double[] adouble1 = this.getMovementOffsets(12 + k * 2, 1.0F);
                    float f21 = this.rotationYaw * 0.017453292F + this.simplifyAngle(adouble1[0] - adouble[0]) * 0.017453292F;
                    float f6 = MathHelper.sin(f21);
                    float f22 = MathHelper.cos(f21);
                    float f23 = 1.5F;
                    float f24 = (float)(k + 1) * 2.0F;
                    multipartentitypart.onUpdate();
                    multipartentitypart.setLocationAndAngles(this.posX - (double)((f3 * 1.5F + f6 * f24) * f16), this.posY + (adouble1[1] - adouble[1]) - (double)((f24 + 1.5F) * f2) + 1.5D, this.posZ + (double)((f18 * 1.5F + f22 * f24) * f16), 0.0F, 0.0F);
                }

                if (this.fightManager != null) {
                    this.fightManager.dragonUpdate(this);
                }

                for(int l = 0; l < this.dragonPartArray.length; ++l) {
                    this.dragonPartArray[l].prevPosX = avec3d[l].x;
                    this.dragonPartArray[l].prevPosY = avec3d[l].y;
                    this.dragonPartArray[l].prevPosZ = avec3d[l].z;
                }

            }
        }

        this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());
    }

    private float getHeadYOffset(float p_184662_1_) {
        double d0;
        if (this.phaseManager.getCurrentPhase().getIsStationary()) {
            d0 = -1.0D;
        } else {
            double[] adouble = this.getMovementOffsets(5, 1.0F);
            double[] adouble1 = this.getMovementOffsets(0, 1.0F);
            d0 = adouble[1] - adouble1[1];
        }

        return (float)d0;
    }

    /**
     * Updates the state of the enderdragon's current endercrystal.
     */
    private void updateDragonEnderCrystal() {
        if (this.healingEnderCrystal != null) {
            if (this.healingEnderCrystal.isDead) {
                this.healingEnderCrystal = null;
            } else if (this.ticksExisted % 10 == 0 && this.getHealth() < this.getMaxHealth()) {
                this.setHealth(this.getHealth() + 1.0F);
            }
        }

        if (this.rand.nextInt(10) == 0) {
            List<EntityEnderCrystal> list = this.world.<EntityEnderCrystal>getEntitiesWithinAABB(EntityEnderCrystal.class, this.getEntityBoundingBox().grow(32.0D));
            EntityEnderCrystal entityendercrystal = null;
            double d0 = Double.MAX_VALUE;

            for(EntityEnderCrystal entityendercrystal1 : list) {
                double d1 = entityendercrystal1.getDistanceSq(this);
                if (d1 < d0) {
                    d0 = d1;
                    entityendercrystal = entityendercrystal1;
                }
            }

            this.healingEnderCrystal = entityendercrystal;
        }

    }

    /**
     * Pushes all entities inside the list away from the enderdragon.
     */
    private void collideWithEntities(List<Entity> p_70970_1_) {
        double d0 = (this.dragonPartBody.getEntityBoundingBox().minX + this.dragonPartBody.getEntityBoundingBox().maxX) / 2.0D;
        double d1 = (this.dragonPartBody.getEntityBoundingBox().minZ + this.dragonPartBody.getEntityBoundingBox().maxZ) / 2.0D;

        for(Entity entity : p_70970_1_) {
            if (entity instanceof EntityLivingBase) {
                // don't push laser turret
                if (this.guardian != null && entity.isEntityEqual(this.guardian)) continue;
                // don't push invulnerable entities
                if (entity.getIsInvulnerable()) continue;
                // don't push other PenguinDungeon mobs
                if (((org.spongepowered.api.entity.Entity) entity).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent()) continue;
                double d2 = entity.posX - d0;
                double d3 = entity.posZ - d1;
                double d4 = d2 * d2 + d3 * d3;
                entity.addVelocity(d2 / d4 * 4.0D, 0.20000000298023224D, d3 / d4 * 4.0D);
                if (!this.phaseManager.getCurrentPhase().getIsStationary() && ((EntityLivingBase)entity).getRevengeTimer() < entity.ticksExisted - 2) {
                    entity.attackEntityFrom(DamageSource.causeMobDamage(this), 5.0F);
                    this.applyEnchantments(this, entity);
                }
            }
        }

    }

    /**
     * Attacks all entities inside this list, dealing 5 hearts of damage.
     */
    private void attackEntitiesInList(List<Entity> p_70971_1_) {
        for(int i = 0; i < p_70971_1_.size(); ++i) {
            Entity entity = p_70971_1_.get(i);
            if (entity instanceof EntityLivingBase) {
                // don't hurt laser turret
                if (this.guardian != null && entity.isEntityEqual(this.guardian)) continue;
                // don't hurt other PenguinDungeon mobs
                if (((org.spongepowered.api.entity.Entity) entity).get(PenguinDungeonKeys.PD_ENTITY_TYPE).isPresent()) continue;
                entity.attackEntityFrom(DamageSource.causeMobDamage(this), 10.0F);
                this.applyEnchantments(this, entity);
            }
        }

    }

    /**
     * Simplifies the value of a number by adding/subtracting 180 to the point that the number is between -180 and 180.
     */
    private float simplifyAngle(double p_70973_1_) {
        return (float)MathHelper.wrapDegrees(p_70973_1_);
    }

    /**
     * Generates values for the fields pathPoints, and neighbors, and then returns the nearest pathPoint to the specified
     * position.
     */
    @Override
    public int initPathPoints() {
        if (this.pathPoints[0] == null) {
            for(int i = 0; i < 24; ++i) {
                int j = 5;
                int l;
                int i1;
                if (i < 12) {
                    l = (int)(60.0F * MathHelper.cos(2.0F * (-3.1415927F + 0.2617994F * (float)i)));
                    i1 = (int)(60.0F * MathHelper.sin(2.0F * (-3.1415927F + 0.2617994F * (float)i)));
                } else if (i < 20) {
                    int lvt_3_1_ = i - 12;
                    l = (int)(40.0F * MathHelper.cos(2.0F * (-3.1415927F + 0.3926991F * (float)lvt_3_1_)));
                    i1 = (int)(40.0F * MathHelper.sin(2.0F * (-3.1415927F + 0.3926991F * (float)lvt_3_1_)));
                    j += 10;
                } else {
                    int k1 = i - 20;
                    l = (int)(20.0F * MathHelper.cos(2.0F * (-3.1415927F + 0.7853982F * (float)k1)));
                    i1 = (int)(20.0F * MathHelper.sin(2.0F * (-3.1415927F + 0.7853982F * (float)k1)));
                }

                int x = this.podium.getX() + l;
                int z = this.podium.getZ() + i1;
                int j1 = this.podium.getY() + j;
                this.pathPoints[i] = new PathPoint(x, j1, z);
            }

            this.neighbors[0] = 6146;
            this.neighbors[1] = 8197;
            this.neighbors[2] = 8202;
            this.neighbors[3] = 16404;
            this.neighbors[4] = 32808;
            this.neighbors[5] = 32848;
            this.neighbors[6] = 65696;
            this.neighbors[7] = 131392;
            this.neighbors[8] = 131712;
            this.neighbors[9] = 263424;
            this.neighbors[10] = 526848;
            this.neighbors[11] = 525313;
            this.neighbors[12] = 1581057;
            this.neighbors[13] = 3166214;
            this.neighbors[14] = 2138120;
            this.neighbors[15] = 6373424;
            this.neighbors[16] = 4358208;
            this.neighbors[17] = 12910976;
            this.neighbors[18] = 9044480;
            this.neighbors[19] = 9706496;
            this.neighbors[20] = 15216640;
            this.neighbors[21] = 13688832;
            this.neighbors[22] = 11763712;
            this.neighbors[23] = 8257536;
        }

        return this.getNearestPpIdx(this.posX, this.posY, this.posZ);
    }

    /**
     * Returns the index into pathPoints of the nearest PathPoint.
     */
    @Override
    public int getNearestPpIdx(double x, double y, double z) {
        float f = 10000.0F;
        int i = 0;
        PathPoint pathpoint = new PathPoint(MathHelper.floor(x), MathHelper.floor(y), MathHelper.floor(z));
        int j = 0;
        if (this.fightManager != null && this.fightManager.getNumAliveCrystals() == 0 ||
           this.fightManager == null && this.getHealth() > (this.getMaxHealth() * 0.3)) {
            j = 12;
        }

        for(int k = j; k < 24; ++k) {
            if (this.pathPoints[k] != null) {
                float f1 = this.pathPoints[k].distanceToSquared(pathpoint);
                if (f1 < f) {
                    f = f1;
                    i = k;
                }
            }
        }

        return i;
    }

    /**
     * Find and return a path among the circles described by pathPoints, or null if the shortest path would just be
     * directly between the start and finish with no intermediate points.
     *
     * Starting with pathPoint[startIdx], it searches the neighboring points (and their neighboring points, and so on)
     * until it reaches pathPoint[finishIdx], at which point it calls makePath to seal the deal.
     */
    @Nullable
    @Override
    public Path findPath(int startIdx, int finishIdx, @Nullable PathPoint andThen) {
        for(int i = 0; i < 24; ++i) {
            PathPoint pathpoint = this.pathPoints[i];
            pathpoint.visited = false;
            pathpoint.distanceToTarget = 0.0F;
            pathpoint.totalPathDistance = 0.0F;
            pathpoint.distanceToNext = 0.0F;
            pathpoint.previous = null;
            pathpoint.index = -1;
        }

        PathPoint pathpoint4 = this.pathPoints[startIdx];
        PathPoint pathpoint5 = this.pathPoints[finishIdx];
        pathpoint4.totalPathDistance = 0.0F;
        pathpoint4.distanceToNext = pathpoint4.distanceTo(pathpoint5);
        pathpoint4.distanceToTarget = pathpoint4.distanceToNext;
        this.pathFindQueue.clearPath();
        this.pathFindQueue.addPoint(pathpoint4);
        PathPoint pathpoint1 = pathpoint4;
        int j = 0;
        if (this.fightManager != null && this.fightManager.getNumAliveCrystals() == 0 ||
            this.fightManager == null && this.getHealth() > (this.getMaxHealth() * 0.3)) {
             j = 12;
         }

        while(!this.pathFindQueue.isPathEmpty()) {
            PathPoint pathpoint2 = this.pathFindQueue.dequeue();
            if (pathpoint2.equals(pathpoint5)) {
                if (andThen != null) {
                    andThen.previous = pathpoint5;
                    pathpoint5 = andThen;
                }

                return this.makePath(pathpoint4, pathpoint5);
            }

            if (pathpoint2.distanceTo(pathpoint5) < pathpoint1.distanceTo(pathpoint5)) {
                pathpoint1 = pathpoint2;
            }

            pathpoint2.visited = true;
            int k = 0;

            for(int l = 0; l < 24; ++l) {
                if (this.pathPoints[l] == pathpoint2) {
                    k = l;
                    break;
                }
            }

            for(int i1 = j; i1 < 24; ++i1) {
                if ((this.neighbors[k] & 1 << i1) > 0) {
                    PathPoint pathpoint3 = this.pathPoints[i1];
                    if (!pathpoint3.visited) {
                        float f = pathpoint2.totalPathDistance + pathpoint2.distanceTo(pathpoint3);
                        if (!pathpoint3.isAssigned() || f < pathpoint3.totalPathDistance) {
                            pathpoint3.previous = pathpoint2;
                            pathpoint3.totalPathDistance = f;
                            pathpoint3.distanceToNext = pathpoint3.distanceTo(pathpoint5);
                            if (pathpoint3.isAssigned()) {
                                this.pathFindQueue.changeDistance(pathpoint3, pathpoint3.totalPathDistance + pathpoint3.distanceToNext);
                            } else {
                                pathpoint3.distanceToTarget = pathpoint3.totalPathDistance + pathpoint3.distanceToNext;
                                this.pathFindQueue.addPoint(pathpoint3);
                            }
                        }
                    }
                }
            }
        }

        if (pathpoint1 == pathpoint4) {
            return null;
        } else {
            LOGGER.debug("Failed to find path from {} to {}", Integer.valueOf(startIdx), Integer.valueOf(finishIdx));
            if (andThen != null) {
                andThen.previous = pathpoint1;
                pathpoint1 = andThen;
            }

            return this.makePath(pathpoint4, pathpoint1);
        }
    }

    /**
     * Create and return a new PathEntity defining a path from the start to the finish, using the connections already
     * made by the caller, findPath.
     */
    private Path makePath(PathPoint start, PathPoint finish) {
        int i = 1;

        for(PathPoint pathpoint = finish; pathpoint.previous != null; pathpoint = pathpoint.previous) {
            ++i;
        }

        PathPoint[] apathpoint = new PathPoint[i];
        PathPoint pathpoint1 = finish;
        --i;

        for(apathpoint[i] = finish; pathpoint1.previous != null; apathpoint[i] = pathpoint1) {
            pathpoint1 = pathpoint1.previous;
            --i;
        }

        return new Path(apathpoint);
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    @Override
    public void writeEntityToNBT(NBTTagCompound compound) {
        super.writeEntityToNBT(compound);
        compound.setInteger("DragonPhase", this.phaseManager.getCurrentPhase().getCustomType().getId());
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    @Override
    public void readEntityFromNBT(NBTTagCompound compound) {
        super.readEntityFromNBT(compound);
        if (compound.hasKey("DragonPhase")) {
            this.phaseManager.setPhase(CustomPhaseList.getById(compound.getInteger("DragonPhase")));
        }
        if (this.hasCustomName()) {
            this.bossInfo.setName(this.getDisplayName());
        }
    }

    /**
     * Sets the custom name tag for this entity
     */
    public void setCustomNameTag(String name) {
        super.setCustomNameTag(name);
        this.bossInfo.setName(this.getDisplayName());
    }

    @Override
    public Vec3d getHeadLookVec(float p_184665_1_) {
        CustomIPhase iphase = this.phaseManager.getCurrentPhase();
        CustomPhaseList<? extends CustomIPhase> phaselist = iphase.getCustomType();
        Vec3d vec3d;
        if (phaselist != CustomPhaseList.LANDING && phaselist != CustomPhaseList.TAKEOFF) {
            if (iphase.getIsStationary()) {
                float f4 = this.rotationPitch;
                float f5 = 1.5F;
                this.rotationPitch = -45.0F;
                vec3d = this.getLook(p_184665_1_);
                this.rotationPitch = f4;
            } else {
                vec3d = this.getLook(p_184665_1_);
            }
        } else {
            BlockPos blockpos = this.podium;
            float f = Math.max(MathHelper.sqrt(this.getDistanceSqToCenter(blockpos)) / 4.0F, 1.0F);
            float f1 = 6.0F / f;
            float f2 = this.rotationPitch;
            float f3 = 1.5F;
            this.rotationPitch = -f1 * 1.5F * 5.0F;
            vec3d = this.getLook(p_184665_1_);
            this.rotationPitch = f2;
        }

        return vec3d;
    }

    @Override
    public boolean attackEntityFromPart(MultiPartEntityPart part, DamageSource source, float damage) {
        damage = this.phaseManager.getCurrentPhase().getAdjustedDamage(part, source, damage);
        if (part != this.dragonPartHead) {
            damage = damage / 4.0F + Math.min(damage, 1.0F);
        }

        if (damage < 0.01F) {
            return false;
        } else {
            if (source.getTrueSource() instanceof EntityPlayer || source.isExplosion()) {
                float f = this.getHealth();
                this.attackDragonFrom(source, damage);
                if (!this.phaseManager.getCurrentPhase().getIsStationary()) {
                    if (this.getHealth() <= 0.0F) {
                        this.setHealth(1.0F);
                        this.phaseManager.setPhase(CustomPhaseList.DYING);
                    } else if (this.alternateForm != null && this.getHealth() <= (this.getMaxHealth() / 4)
                            && this.attackTypes.contains(AttackType.CHANGE_FORM)) {
                        if (this.phaseManager.getCurrentPhase() != CustomPhaseList.LANDING &&
                            this.phaseManager.getCurrentPhase() != CustomPhaseList.LANDING_APPROACH) {
                            this.phaseManager.setPhase(CustomPhaseList.LANDING_APPROACH);
                        }
                    }
                } else if (this.alternateForm != null && this.getHealth() <= (this.getMaxHealth() / 4)
                        && this.attackTypes.contains(AttackType.CHANGE_FORM)) {
                    float f12 = (this.rand.nextFloat() - 0.5F) * 8.0F;
                    float f13 = (this.rand.nextFloat() - 0.5F) * 4.0F;
                    float f15 = (this.rand.nextFloat() - 0.5F) * 8.0F;
                    this.world.spawnParticle(EnumParticleTypes.EXPLOSION_LARGE, this.posX + (double)f12, this.posY + 2.0D + (double)f13, this.posZ + (double)f15, 0.0D, 0.0D, 0.0D);
                    this.playSound(getSound(), 5.0F, 1.0F);
                    // replace with alternate form
                    this.alternateForm.createEntity((org.spongepowered.api.world.World) this.getWorld(),
                            new Vector3d(this.getPosition().getX(), this.getPosition().getY(), this.getPosition().getZ()))
                            .spawn((org.spongepowered.api.world.World) this.getWorld());
                    this.setDead();
                } else {
                    this.sittingDamageReceived = (int)((float)this.sittingDamageReceived + (f - this.getHealth()));
                    if ((float)this.sittingDamageReceived > 0.25F * this.getMaxHealth()) {
                        this.sittingDamageReceived = 0;
                        this.phaseManager.setPhase(CustomPhaseList.TAKEOFF);
                    }
                }
            }

            return true;
        }
    }

    /**
     * Called when the entity is attacked.
     */
    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
       if (source instanceof EntityDamageSource && ((EntityDamageSource)source).getIsThornsDamage()) {
          this.attackEntityFromPart(this.dragonPartBody, source, amount);
       }

       return false;
    }

    /**
     * Add the given player to the list of players tracking this entity. For instance, a player may track a boss in order
     * to view its associated boss bar.
     */
    public void addTrackingPlayer(EntityPlayerMP player) {
        super.addTrackingPlayer(player);
        this.bossInfo.addPlayer(player);
    }

    /**
     * Removes the given player from the list of players tracking this entity. See {@link Entity#addTrackingPlayer} for
     * more information on tracking.
     */
    public void removeTrackingPlayer(EntityPlayerMP player) {
        super.removeTrackingPlayer(player);
        this.bossInfo.removePlayer(player);
    }

    public CustomPhaseManager getPhaseManager() {
        return this.phaseManager;
    }

    @Nullable
    public DragonFightManager getFightManager() {
       return this.fightManager;
    }

    @Override
    public void onDeath(DamageSource cause) {
        super.onDeath(cause);
        if (!(this.podium.getX() == 0 && this.podium.getZ() == 0)) {
            Optional<ResourceKey> pdEntityType = ((org.spongepowered.api.entity.Entity) this).get(PenguinDungeonKeys.PD_ENTITY_TYPE);
            // Astral Dragons leave behind gateways to the end spawn spot
            if (pdEntityType.isPresent() && pdEntityType.get().equals(CustomEntityTypes.ASTRAL_DRAGON.getId())) {
                Optional<org.spongepowered.api.world.World> the_end = Sponge.getServer().getWorld("the_end");
                if (!the_end.isPresent()) the_end = Sponge.getServer().getWorld("DIM1");
                if (the_end.isPresent() && the_end.get().equals((org.spongepowered.api.world.World)this.world)) {
                    this.world.setBlockState(this.podium, Blocks.END_GATEWAY.getDefaultState());
                    TileEntity tileentity = this.world.getTileEntity(this.podium);
                    if (tileentity instanceof TileEntityEndGateway) {
                       TileEntityEndGateway tileentityendgateway = (TileEntityEndGateway)tileentity;
                       tileentityendgateway.setExactPosition(new BlockPos(100, 49, 0)); // above the end obsidian spawn platform
                       tileentityendgateway.markDirty();
                    }
                }
            }
        }
    }
}
