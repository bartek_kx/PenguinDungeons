package com.minecraftonline.penguindungeons.customentity;

import java.util.Optional;

import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.ai.task.AITask;
import org.spongepowered.api.entity.living.Agent;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

import static com.minecraftonline.penguindungeons.customentity.CustomEntityType.TICKS_PER_SECOND;

import com.minecraftonline.penguindungeons.ai.AIUtil;
import com.minecraftonline.penguindungeons.ai.DelegatingToMCAI;
import com.minecraftonline.penguindungeons.ai.AIUtil.DirectShulkerBulletCreator;
import com.minecraftonline.penguindungeons.ai.AIUtil.ProjectileCreator;
import com.minecraftonline.penguindungeons.ai.AnyProjectile;
import com.minecraftonline.penguindungeons.data.PenguinDungeonAITaskTypes;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.monster.EntityBlaze;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityShulkerBullet;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

public class StraightProjectileAttack<T extends EntityLiving> extends EntityAIBase {

    /** The entity the AI instance has been applied to */
    private final EntityLiving entityHost;
    /** The entity (as a RangedAttackMob) the AI instance has been applied to. */
    private final Living rangedAttackEntityHost;
    private EntityLivingBase attackTarget;
    /**
     * A decrementing tick that spawns a ranged attack once this value reaches 0. It is then set back to the
     * maxRangedAttackTime.
     */
    private int rangedAttackTime;
    private final double entityMoveSpeed;
    private int seeTime;
    private final int attackIntervalMin;
    /** The maximum time the AI has to wait before performing another ranged attack. */
    private final int maxRangedAttackTime;
    private final float attackRadius;
    private final float maxAttackDistance;
    private Optional<String> projectileName = Optional.empty();
    private DirectShulkerBulletCreator bulletCreator = null;
    private ProjectileCreator projectileCreator = null;

    public StraightProjectileAttack(Living attacker) {
        this(attacker, 1.25D, 25, 45, 15.0F);
    }

    public StraightProjectileAttack(Living attacker, double movespeed, int maxAttackTime, float maxAttackDistanceIn) {
       this(attacker, movespeed, maxAttackTime, maxAttackTime, maxAttackDistanceIn);
    }

    public StraightProjectileAttack(Living attacker, double movespeed, int p_i1650_4_, int maxAttackTime, float maxAttackDistanceIn) {
       this.rangedAttackTime = -1;
       if (!(attacker instanceof EntityLivingBase)) {
          throw new IllegalArgumentException("AttackGoal requires Mob implements EntityLivingBase");
       } else {
          this.rangedAttackEntityHost = attacker;
          this.entityHost = (EntityLiving)attacker;
          this.entityMoveSpeed = movespeed;
          this.attackIntervalMin = p_i1650_4_;
          this.maxRangedAttackTime = maxAttackTime;
          this.attackRadius = maxAttackDistanceIn;
          this.maxAttackDistance = maxAttackDistanceIn * maxAttackDistanceIn;
          if (this.rangedAttackEntityHost instanceof EntityGhast) {
              // don't stop ghast looking/moving task
              this.setMutexBits(4);
          } else {
              this.setMutexBits(3);
          }
       }
    }

    public StraightProjectileAttack<T> setProjectileCreator(ProjectileCreator creator)
    {
        projectileCreator = creator;
        bulletCreator = null;
        return this;
    }

    public StraightProjectileAttack<T> setDirectShulkerBulletCreator(DirectShulkerBulletCreator creator)
    {
        projectileCreator = null;
        bulletCreator = creator;
        return this;
    }

    @SuppressWarnings("deprecation")
    public StraightProjectileAttack<T> setName(Optional<Text> name)
    {
        projectileName = name.map(TextSerializers.LEGACY_FORMATTING_CODE::serialize);
        return this;
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute() {
       EntityLivingBase entitylivingbase = this.entityHost.getAttackTarget();
       if (entitylivingbase == null || !entitylivingbase.isEntityAlive()) {
          return false;
       } else {
          this.attackTarget = entitylivingbase;
          return true;
       }
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean shouldContinueExecuting() {
       return this.shouldExecute() || !this.entityHost.getNavigator().noPath();
    }

    /**
     * Reset the task's internal state. Called when this task is interrupted by another one
     */
    public void resetTask() {
       this.attackTarget = null;
       this.seeTime = 0;
       this.rangedAttackTime = -1;
       if (this.rangedAttackEntityHost instanceof EntityGhast) {
           ((EntityGhast) this.rangedAttackEntityHost).setAttacking(false);
       }
    }

    /**
     * Keep ticking a continuous task that has already been started
     */
    public void updateTask() {
       double d0 = this.entityHost.getDistanceSq(this.attackTarget.posX, this.attackTarget.getEntityBoundingBox().minY, this.attackTarget.posZ);
       boolean flag = this.entityHost.getEntitySenses().canSee(this.attackTarget);
       if (flag) {
          ++this.seeTime;
       } else {
          this.seeTime = 0;
       }

       // ghasts have their own looking and moving tasks
       if (!(this.rangedAttackEntityHost instanceof EntityGhast)) {
           if (d0 <= (double)this.maxAttackDistance && this.seeTime >= 20) {
              this.entityHost.getNavigator().clearPath();
           } else {
              this.entityHost.getNavigator().tryMoveToEntityLiving(this.attackTarget, this.entityMoveSpeed);
           }
           this.entityHost.getLookHelper().setLookPositionWithEntity(this.attackTarget, 30.0F, 30.0F);
       }
       if (--this.rangedAttackTime == 0) {
          if (!flag) {
             return;
          }

          float f = MathHelper.sqrt(d0) / this.attackRadius;

          double d1 = this.attackTarget.posX - this.entityHost.posX;
          double d2 = this.attackTarget.getEntityBoundingBox().minY + (double)(this.attackTarget.height / 2.0F) - (this.entityHost.posY + (double)(this.entityHost.height / 2.0F));
          double d3 = this.attackTarget.posZ - this.entityHost.posZ;
          @SuppressWarnings("unchecked")
          T living = (T) this.rangedAttackEntityHost;
          if (bulletCreator != null)
          {
              EntityShulkerBullet entityshulkerbullet = bulletCreator.create(living.world, living, this.attackTarget, d1, d2, d3);
              if (projectileName.isPresent()) entityshulkerbullet.setCustomNameTag(projectileName.get());
              living.world.spawnEntity(entityshulkerbullet);
          }
          else if (projectileCreator != null) {
              AnyProjectile projectile = projectileCreator.create(living.world, living, d1, d2, d3);
              if (projectileName.isPresent()) projectile.getMinecraftEntity().setCustomNameTag(projectileName.get());
              if (projectile.getMinecraftEntity() instanceof EntityThrowable)
              {
                  ((EntityThrowable) projectile.getMinecraftEntity()).shoot(d1, d2 + (double)f, d3, 1.6F, 12.0F);
              }
              living.world.spawnEntity(projectile.getMinecraftEntity());
          }
          this.rangedAttackTime = MathHelper.floor(f * (float)(this.maxRangedAttackTime - this.attackIntervalMin) + (float)this.attackIntervalMin);
          if (this.rangedAttackEntityHost instanceof EntityGhast) {
              // ghast shooting
              ((EntityGhast) this.rangedAttackEntityHost).world.playEvent((EntityPlayer)null, 1016, new BlockPos(((EntityGhast) this.rangedAttackEntityHost)), 0);
              ((EntityGhast) this.rangedAttackEntityHost).setAttacking(false);
          } else if (this.rangedAttackEntityHost instanceof EntityBlaze) {
              // blaze shooting
              ((EntityBlaze) this.rangedAttackEntityHost).world.playEvent((EntityPlayer)null, 1018, new BlockPos(((EntityBlaze) this.rangedAttackEntityHost)), 0);
          } // no need for dragon shoot event here as CustomDragon already fires it
       } else if (this.rangedAttackTime < 0) {
          float f2 = MathHelper.sqrt(d0) / this.attackRadius;
          this.rangedAttackTime = MathHelper.floor(f2 * (float)(this.maxRangedAttackTime - this.attackIntervalMin) + (float)this.attackIntervalMin);
       } else if (this.rangedAttackTime == 10 && this.rangedAttackEntityHost instanceof EntityGhast) {
           // ghast warning
           ((EntityGhast) this.rangedAttackEntityHost).world.playEvent((EntityPlayer)null, 1015, new BlockPos(((EntityGhast) this.rangedAttackEntityHost)), 0);
           ((EntityGhast) this.rangedAttackEntityHost).setAttacking(true);
       }

    }

    public static class RangedAttack extends DelegatingToMCAI<Agent> {

        public RangedAttack(Living living) {
            this(living, Optional.empty());
        }

        public RangedAttack(Living living, Optional<Text> name) {
            // default slowness with glass break sound
            this(living, name, DirectShulkerBulletCreator.forDirectBullet(
                    AIUtil.ShulkerBulletCreator.forHit(PotionEffect.of(PotionEffectTypes.SLOWNESS, 1, TICKS_PER_SECOND*10))));
        }

        public RangedAttack(Living living, Optional<Text> name, ProjectileCreator creator) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityLiving>(living)
                    .setName(name)
                    .setProjectileCreator(creator));
        }

        public RangedAttack(Living living, Optional<Text> name, ProjectileCreator creator,
                double movespeed, int maxAttackTime, float maxAttackDistance) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityLiving>(living, movespeed, maxAttackTime, maxAttackDistance)
                    .setName(name)
                    .setProjectileCreator(creator));
        }

        public RangedAttack(Living living, Optional<Text> name, ProjectileCreator creator,
                double movespeed, int minAttackTime, int maxAttackTime, float maxAttackDistance) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityLiving>(living, movespeed, minAttackTime, maxAttackTime, maxAttackDistance)
                    .setName(name)
                    .setProjectileCreator(creator));
        }

        public RangedAttack(Living living, Optional<Text> name, DirectShulkerBulletCreator creator) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityLiving>(living)
                    .setName(name)
                    .setDirectShulkerBulletCreator(creator));
        };

        public RangedAttack(Living living, Optional<Text> name, DirectShulkerBulletCreator creator,
                double movespeed, int maxAttackTime, float maxAttackDistance) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityLiving>(living, movespeed, maxAttackTime, maxAttackDistance)
                    .setName(name)
                    .setDirectShulkerBulletCreator(creator));
        }

        public RangedAttack(Living living, Optional<Text> name, DirectShulkerBulletCreator creator,
                double movespeed, int minAttackTime, int maxAttackTime, float maxAttackDistance) {
            super(PenguinDungeonAITaskTypes.STRAIGHT_PROJECTILE_RANGED_ATTACK,
                    new StraightProjectileAttack<EntityLiving>(living, movespeed, minAttackTime, maxAttackTime, maxAttackDistance)
                    .setName(name)
                    .setDirectShulkerBulletCreator(creator));
        }

        @Override
        public boolean canRunConcurrentWith(AITask<Agent> other) {
            return true;
        }
    }
}