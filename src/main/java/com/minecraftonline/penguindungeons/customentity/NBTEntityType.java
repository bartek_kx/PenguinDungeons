package com.minecraftonline.penguindungeons.customentity;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.spawnable.Spawnable;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import net.minecraft.init.Items;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.chunk.storage.AnvilChunkLoader;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityArchetype;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.world.World;
import org.spongepowered.common.data.persistence.NbtTranslator;

import java.util.Optional;

public class NBTEntityType implements PDEntityType {

    private final ResourceKey key;
    private final EntityType entityType;
    private DataContainer dataContainer;
    private NBTTagCompound nbt;

    public NBTEntityType(ResourceKey key, final EntityType entityType, final DataContainer eggNbt) {
        this.key = key;
        this.entityType = entityType;
        this.setNbt(eggNbt);
    }

    @Override
    public ResourceKey getId() {
        return this.key;
    }

    @Override
    public EntityType getType() {
        return this.entityType;
    }

    @Override
    public ItemStack getSpawnEgg() {
        if (this.entityType == EntityTypes.FIREWORK) {
            return FireworkHelper.toFireworkItem(this.getEntityArchetype().get().getEntityData()).createStack();
        } else {
            net.minecraft.item.ItemStack item = new net.minecraft.item.ItemStack(Items.SPAWN_EGG);
            item.setTagCompound(this.nbt);
            return (ItemStack) (Object) item;
        }
    }

    @Override
    public Spawnable createEntity(World world, Vector3d pos) {
        return w -> {
            // How mc does it with /summon.
            NBTTagCompound entityTag = this.nbt.getCompoundTag("EntityTag").copy();
            net.minecraft.entity.Entity entity = AnvilChunkLoader.readWorldEntityPos(entityTag, (net.minecraft.world.World) world, pos.getX(), pos.getY(), pos.getZ(), true);
            return (Entity) entity;
        };
    }

    @Override
    public Optional<EntityArchetype> getEntityArchetype() {
        DataView entityTag = this.dataContainer.getView(DataQuery.of("EntityTag")).orElse(DataContainer.createNew());
        return Optional.of(EntityArchetype.builder()
                .type(this.entityType)
                .entityData(entityTag)
                .build());
    }

    public EntityType getEntityType() {
        return this.entityType;
    }

    public DataContainer getNbt() {
        return this.dataContainer;
    }

    public void setNbt(DataContainer nbt) {
        this.dataContainer = nbt;
        this.nbt = NbtTranslator.getInstance().translate(nbt);
    }
}
