package com.minecraftonline.penguindungeons.customentity.wither;

import java.util.Arrays;
import java.util.List;

import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;

import static com.minecraftonline.penguindungeons.customentity.CustomEntityType.TICKS_PER_SECOND;

import com.minecraftonline.penguindungeons.ai.AIUtil.ProjectileCreator;
import com.minecraftonline.penguindungeons.ai.AnyProjectile;
import com.minecraftonline.penguindungeons.customentity.HasProjectileData;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntityWitherSkull;
import net.minecraft.init.MobEffects;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntitySelectors;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.BossInfo;
import net.minecraft.world.BossInfoServer;
import net.minecraft.world.World;

public class CustomWither extends EntityWither implements HasProjectileData {
    private final int[] nextHeadUpdate = new int[2];
    private final int[] idleHeadUpdates = new int[2];
    private List<PotionEffect> effects = Arrays.asList(new PotionEffect(MobEffects.WITHER, TICKS_PER_SECOND*3));
    private SoundEvent ambientSound = SoundEvents.ENTITY_WITHER_AMBIENT;
    private SoundEvent hurtSound = SoundEvents.ENTITY_WITHER_HURT;
    private SoundEvent deathSound = SoundEvents.ENTITY_WITHER_DEATH;
    private EnumParticleTypes particle = EnumParticleTypes.DRAGON_BREATH;
    private int particleParam1 = 0;
    private int particleParam2 = 0;
    private String projectileName = "Skull";
    private ProjectileCreator fireballCreator = (world, shooter, accelX, accelY, accelZ) ->
        new AnyProjectile((org.spongepowered.api.entity.Entity) new EntityWitherSkull(world, shooter, accelX, accelY, accelZ));
    private ProjectileCreator boostedFireballCreator = fireballCreator;
    private final boolean showBossBar;
    private final BossInfoServer bossInfo;

    public CustomWither(World worldIn) {
        this(worldIn, BossInfo.Color.PURPLE);
    }

    public CustomWither(World worldIn, BossInfo.Color bossBarColor) {
        this(worldIn, bossBarColor, true);
    }

    public CustomWither(World worldIn, BossInfo.Color bossBarColor, boolean darkenSky) {
        this(worldIn, bossBarColor, darkenSky, true);
    }

    public CustomWither(World worldIn, BossInfo.Color bossBarColor, boolean darkenSky, boolean showBossBar) {
        super(worldIn);
        this.showBossBar = showBossBar;
        this.bossInfo = (BossInfoServer)(new BossInfoServer(this.getDisplayName(), bossBarColor, BossInfo.Overlay.PROGRESS)).setDarkenSky(darkenSky);
    }

    public CustomWither(World worldIn, boolean showBossBar) {
        super(worldIn);
        this.showBossBar = showBossBar;
        this.bossInfo = (BossInfoServer)(new BossInfoServer(this.getDisplayName(), BossInfo.Color.PURPLE, BossInfo.Overlay.PROGRESS));
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return this.ambientSound;
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        return this.hurtSound;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return this.deathSound;
    }

    public void setAmbientSound(SoundEvent sound) {
        this.ambientSound = sound;
    }

    public void setHurtSound(SoundEvent sound) {
        this.hurtSound = sound;
    }

    public void setDeathSound(SoundEvent sound) {
        this.deathSound = sound;
    }

    private double getHeadX(int p_82214_1_) {
        if (p_82214_1_ <= 0) {
            return this.posX;
        } else {
            float f = (this.renderYawOffset + (float)(180 * (p_82214_1_ - 1))) * 0.017453292F;
            float f1 = MathHelper.cos(f);
            return this.posX + (double)f1 * 1.3D;
        }
    }

    private double getHeadY(int p_82208_1_) {
        return p_82208_1_ <= 0 ? this.posY + 3.0D : this.posY + 2.2D;
    }

     private double getHeadZ(int p_82213_1_) {
         if (p_82213_1_ <= 0) {
             return this.posZ;
         } else {
             float f = (this.renderYawOffset + (float)(180 * (p_82213_1_ - 1))) * 0.017453292F;
             float f1 = MathHelper.sin(f);
             return this.posZ + (double)f1 * 1.3D;
         }
     }

    private void launchProjectileToEntity(int p_82216_1_, EntityLivingBase p_82216_2_) {
        this.launchProjectileToCoords(p_82216_1_, p_82216_2_.posX, p_82216_2_.posY + (double)p_82216_2_.getEyeHeight() * 0.5D, p_82216_2_.posZ, p_82216_1_ == 0);
    }

    private void launchProjectileToCoords(int p_82209_1_, double x, double y, double z, boolean boosted) {
        this.world.playEvent((EntityPlayer)null, 1024, new BlockPos(this), 0);
        double d0 = this.getHeadX(p_82209_1_);
        double d1 = this.getHeadY(p_82209_1_);
        double d2 = this.getHeadZ(p_82209_1_);
        double d3 = x - d0;
        double d4 = y - d1;
        double d5 = z - d2;
        AnyProjectile projectile;
        if (boosted)
        {
            projectile = boostedFireballCreator.create(this.world, this, d3, d4, d5);
            if (projectile.getMinecraftEntity() instanceof EntityWitherSkull)
            {
                ((EntityWitherSkull)projectile.getMinecraftEntity()).setInvulnerable(boosted);
            }
        }
        else
        {
            projectile = fireballCreator.create(this.world, this, d3, d4, d5);
        }
        projectile.getMinecraftEntity().setCustomNameTag(this.projectileName);

        projectile.getMinecraftEntity().posY = d1;
        projectile.getMinecraftEntity().posX = d0;
        projectile.getMinecraftEntity().posZ = d2;
        this.world.spawnEntity(projectile.getMinecraftEntity());
    }

    @Override
    public void attackEntityWithRangedAttack(EntityLivingBase target, float distanceFactor) {
        this.launchProjectileToEntity(0, target);
    }

    protected void updateAITasks() {
        if (this.getInvulTime() > 0) {
            int j1 = this.getInvulTime() - 1;
            if (j1 <= 0) {
                this.world.newExplosion(this, this.posX, this.posY + (double)this.getEyeHeight(), this.posZ, 7.0F, false, this.world.getGameRules().getBoolean("mobGriefing"));
                this.world.playBroadcastSound(1023, new BlockPos(this), 0);
            }

            this.setInvulTime(j1);
            if (this.ticksExisted % 10 == 0) {
                this.heal(10.0F);
            }

        } else {
            // don't update parent tasks here as it will just repeat this
            //super.updateAITasks();

            for(int i = 1; i < 3; ++i) {
                if (this.ticksExisted >= this.nextHeadUpdate[i - 1]) {
                    this.nextHeadUpdate[i - 1] = this.ticksExisted + 10 + this.rand.nextInt(10);
                    int j3 = i - 1;
                    this.idleHeadUpdates[j3] = this.idleHeadUpdates[i - 1] + 1;

                    int k1 = this.getWatchedTargetId(i);
                    if (k1 > 0) {
                        Entity entity = this.world.getEntityByID(k1);
                        if (entity != null && entity.isEntityAlive() && this.getDistanceSq(entity) <= 900.0D && this.canEntityBeSeen(entity)) {
                            if (entity instanceof EntityPlayer && ((EntityPlayer)entity).capabilities.disableDamage) {
                                this.updateWatchedTargetId(i, 0);
                            } else {
                                this.launchProjectileToEntity(i + 1, (EntityLivingBase)entity);
                                this.nextHeadUpdate[i - 1] = this.ticksExisted + 40 + this.rand.nextInt(20);
                                this.idleHeadUpdates[i - 1] = 0;
                            }
                        } else {
                            this.updateWatchedTargetId(i, 0);
                        }
                    } else {
                        List<EntityLivingBase> list = this.world.<EntityLivingBase>getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().grow(20.0D, 8.0D, 20.0D), EntitySelectors.NOT_SPECTATING);

                        for(int j2 = 0; j2 < 10 && !list.isEmpty(); ++j2) {
                            EntityLivingBase entitylivingbase = list.get(this.rand.nextInt(list.size()));
                            if (entitylivingbase != this && entitylivingbase.isEntityAlive() && this.canEntityBeSeen(entitylivingbase)) {
                                if (entitylivingbase instanceof EntityPlayer) {
                                    if (!((EntityPlayer)entitylivingbase).capabilities.disableDamage) {
                                        this.updateWatchedTargetId(i, entitylivingbase.getEntityId());
                                    }
                                } else {
                                    this.updateWatchedTargetId(i, entitylivingbase.getEntityId());
                                }
                                break;
                            }

                            list.remove(entitylivingbase);
                        }
                    }
                }
            }

            if (this.getAttackTarget() != null) {
                this.updateWatchedTargetId(0, this.getAttackTarget().getEntityId());
            } else {
                this.updateWatchedTargetId(0, 0);
            }

            if (this.ticksExisted % 20 == 0) {
                this.heal(1.0F);
            }

            this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());
        }
    }

    @Override
    public void readEntityFromNBT(NBTTagCompound compound) {
        super.readEntityFromNBT(compound);
        if (this.hasCustomName()) {
            this.bossInfo.setName(this.getDisplayName());
        }
    }

    @Override
    public void setCustomNameTag(String name) {
        super.setCustomNameTag(name);
        this.bossInfo.setName(this.getDisplayName());
    }

    @Override
    public void addTrackingPlayer(EntityPlayerMP player) {
        if (this.showBossBar) {
            this.bossInfo.addPlayer(player);
        }
    }

    @Override
    public void removeTrackingPlayer(EntityPlayerMP player) {
        if (this.showBossBar) {
            this.bossInfo.removePlayer(player);
        }
    }

    public void setEffects(List<PotionEffect> effects)
    {
        this.effects = effects;
    }

    public List<PotionEffect> getEffects()
    {
        return effects;
    }

    public void setParticle(EnumParticleTypes particle)
    {
        setParticle(particle, 0, 0);
    }

    public void setParticle(EnumParticleTypes particle, int particleParam1, int particleParam2)
    {
        this.particle = particle;
        this.particleParam1 = particleParam1;
        this.particleParam2 = particleParam2;
    }

    public EnumParticleTypes getParticle()
    {
        return this.particle;
    }

    public int getParticleParam1()
    {
        return this.particleParam1;
    }

    public int getParticleParam2()
    {
        return this.particleParam2;
    }

    public void setProjectileName(String projectileName)
    {
        this.projectileName = projectileName;
    }

    @SuppressWarnings("deprecation")
    public void setProjectileName(Text projectileName)
    {
        this.projectileName = TextSerializers.LEGACY_FORMATTING_CODE.serialize(projectileName);
    }

    public String getProjectileName()
    {
        return this.projectileName;
    }

    public void setProjectileCreator(ProjectileCreator creator)
    {
        this.setProjectileCreators(creator, creator);
    }

    public void setProjectileCreators(ProjectileCreator creator, ProjectileCreator boostedCreator)
    {
        this.fireballCreator = creator;
        this.boostedFireballCreator = boostedCreator;
    }

}
