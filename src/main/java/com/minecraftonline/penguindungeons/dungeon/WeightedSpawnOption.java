package com.minecraftonline.penguindungeons.dungeon;

import com.minecraftonline.penguindungeons.customentity.PDEntityType;
import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

public class WeightedSpawnOption implements SpawnOption {

    private static final int CHECK_NEARBY_ENTITIES_RANGE = 2;

    private final Map<PDEntityType, Integer> entityWeightMap = new LinkedHashMap<>();
    private transient int totalWeight;

    @Override
    public Optional<PDEntityType> next(Random rand) {
        if (totalWeight == 0) {
            return Optional.empty();
        }
        final int r = rand.nextInt(totalWeight);
        int current = 0;
        for (Map.Entry<PDEntityType, Integer> entry : entityWeightMap.entrySet()) {
            current += entry.getValue();
            if (current > r) {
                return Optional.of(entry.getKey());
            }
        }
        throw new IllegalStateException("Couldn't find item in weighted map, for random number: " + r + " in total weight " + totalWeight);
    }

    public Map<PDEntityType, Integer> all() {
        return Collections.unmodifiableMap(this.entityWeightMap);
    }

    public boolean has(PDEntityType customEntityType) {
        return this.entityWeightMap.containsKey(customEntityType);
    }

    public void set(PDEntityType pdEntityType, int weight) {
        this.entityWeightMap.put(pdEntityType, weight);
        this.updateTotalWeight();
    }

    @Nullable
    public Integer getWeight(PDEntityType pdEntityType) {
        return this.entityWeightMap.get(pdEntityType);
    }

    public void remove(PDEntityType pdEntityType) {
        this.entityWeightMap.remove(pdEntityType);
        this.updateTotalWeight();
    }

    private void updateTotalWeight() {
        this.totalWeight = this.entityWeightMap.values().stream()
                .mapToInt(Integer::intValue)
                .sum();
    }

    public int totalWeight() {
        return this.totalWeight;
    }

    @Override
    public boolean shouldSpawn(Location<World> loc) {
        Collection<Entity> nearbyEntities = loc.getExtent().getNearbyEntities(loc.getPosition(), CHECK_NEARBY_ENTITIES_RANGE);
        if (nearbyEntities.size() > 15) {
            return false;
        }

        for (Entity entity : nearbyEntities) {
            ResourceKey key = entity.get(PenguinDungeonKeys.PD_ENTITY_TYPE)
                    .orElseGet(() -> ResourceKey.resolve(entity.getType().getId()));

            if (this.entityWeightMap.keySet().stream()
                    .map(PDEntityType::getId)
                    .anyMatch(otherKey -> otherKey.equals(key))) {
                return false;
            }
        }
        return true;
    }
}
