package com.minecraftonline.penguindungeons.equipment;

import net.minecraft.inventory.EntityEquipmentSlot;
import org.spongepowered.api.entity.living.Living;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.util.Tuple;

import java.util.List;

public interface EquipmentLoadout {

    void apply(Living entity);

    List<Tuple<ItemStack, Double>> getDropChances();

    static Builder builder() {
        return new SimpleEquipmentLoadoutBuilder();
    }

    static EquipmentLoadout empty() {
        return new EmptyEquipmentLoadout();
    }

    interface Builder {

        Builder slot(EntityEquipmentSlot slot, ItemStack itemStack, double dropChance);

        Builder head(ItemStack itemStack, double dropChance);

        Builder chest(ItemStack itemStack, double dropChance);

        Builder leggings(ItemStack itemStack, double dropChance);

        Builder boots(ItemStack itemStack, double dropChance);

        Builder mainHand(ItemStack itemStack, double dropChance);

        Builder offHand(ItemStack itemStack, double dropChance);

        EquipmentLoadout build();
    }
}
