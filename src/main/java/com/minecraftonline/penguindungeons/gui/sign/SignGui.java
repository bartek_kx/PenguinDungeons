package com.minecraftonline.penguindungeons.gui.sign;

import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.common.data.manipulator.mutable.tileentity.SpongeSignData;

import java.util.List;
import java.util.function.BiConsumer;

public class SignGui {

    private final SignData signData;
    private final BiConsumer<Player, String[]> hook;

    public SignGui(List<Text> lines, BiConsumer<Player, String[]> hook) {
        this.hook = hook;
        if (lines.size() != 4) {
            throw new IllegalArgumentException("Must be an array of 4 lines of Text");
        }
        this.signData = new SpongeSignData(lines);
    }

    public SignData getSignData() {
        return signData;
    }

    public BiConsumer<Player, String[]> getHook() {
        return hook;
    }
}
