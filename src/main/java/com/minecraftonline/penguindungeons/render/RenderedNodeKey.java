package com.minecraftonline.penguindungeons.render;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.penguindungeons.dungeon.Dungeon;

import java.util.Objects;

public class RenderedNodeKey {
    private final String dungeonName;
    private final Vector3d pos;

    private RenderedNodeKey(final String dungeonName, final Vector3d pos) {
        this.dungeonName = dungeonName;
        this.pos = pos;
    }

    public static RenderedNodeKey of(final Dungeon dungeon, final Vector3d pos) {
        return new RenderedNodeKey(dungeon.name(), pos);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RenderedNodeKey)) return false;
        RenderedNodeKey that = (RenderedNodeKey) o;
        return dungeonName.equals(that.dungeonName) && pos.equals(that.pos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dungeonName, pos);
    }
}
