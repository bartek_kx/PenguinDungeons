package com.minecraftonline.penguindungeons.data.bossbar;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

import org.spongepowered.api.boss.BossBarColor;
import org.spongepowered.api.boss.BossBarColors;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.manipulator.DataManipulatorBuilder;
import org.spongepowered.api.data.persistence.AbstractDataBuilder;
import org.spongepowered.api.data.persistence.InvalidDataException;

import java.util.Optional;

public class PDBossBarColorDataBuilder extends AbstractDataBuilder<PDBossBarColorData> implements DataManipulatorBuilder<PDBossBarColorData, ImmutablePDBossBarColorData>
{
    public PDBossBarColorDataBuilder()
    {
        super(PDBossBarColorData.class, 1);
    }

    @Override
    public PDBossBarColorData create()
    {
        return new PDBossBarColorData(BossBarColors.RED);
    }

    @Override
    public Optional<PDBossBarColorData> createFrom(DataHolder dataHolder)
    {
        return this.create().fill(dataHolder);
    }

    @Override
    protected Optional<PDBossBarColorData> buildContent(DataView container) throws InvalidDataException
    {
        if (container.contains(PenguinDungeonKeys.PD_BOSS_BAR_COLOR.getQuery()))
        {
            return Optional.of(new PDBossBarColorData(container.getObject(PenguinDungeonKeys.PD_BOSS_BAR_COLOR.getQuery(), BossBarColor.class).get()));
        }

        return Optional.empty();
    }
}

