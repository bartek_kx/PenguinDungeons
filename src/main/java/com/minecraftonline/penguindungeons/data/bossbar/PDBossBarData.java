package com.minecraftonline.penguindungeons.data.bossbar;

import java.util.Optional;

import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractBooleanData;
import org.spongepowered.api.data.merge.MergeFunction;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

public class PDBossBarData extends AbstractBooleanData<PDBossBarData, ImmutablePDBossBarData>
{

	public PDBossBarData(boolean value)
	{
        super(PenguinDungeonKeys.PD_BOSS_BAR, value, false);
    }

    @Override
    public Optional<PDBossBarData> fill(DataHolder dataHolder, MergeFunction overlap)
    {
        dataHolder.get(PDBossBarData.class).ifPresent((data) -> {
            PDBossBarData finalData = overlap.merge(this, data);
            setValue(finalData.getValue());
        });
        return Optional.of(this);
    }

    @Override
    public Optional<PDBossBarData> from(DataContainer container)
    {
        Optional<Boolean> optBoolean = container.getBoolean(this.usedKey.getQuery());
        return optBoolean.map(PDBossBarData::new);
    }

    @Override
    public PDBossBarData copy()
    {
        return new PDBossBarData(this.getValue());
    }

    @Override
    public ImmutablePDBossBarData asImmutable()
    {
        return new ImmutablePDBossBarData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }
}
