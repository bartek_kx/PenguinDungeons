package com.minecraftonline.penguindungeons.data.wand;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.manipulator.mutable.common.AbstractBooleanData;
import org.spongepowered.api.data.merge.MergeFunction;

import java.util.Optional;

public class CustomWandData extends AbstractBooleanData<CustomWandData, ImmutableCustomWandData> {

    public CustomWandData() {
        this(false);
    }

    public CustomWandData(boolean value) {
        super(PenguinDungeonKeys.IS_PD_WAND, value, false);
    }

    @Override
    public Optional<CustomWandData> fill(DataHolder dataHolder, MergeFunction overlap) {
        dataHolder.get(CustomWandData.class).ifPresent((data) -> {
            CustomWandData finalData = overlap.merge(this, data);
            setValue(finalData.getValue());
        });
        return Optional.of(this);
    }

    @Override
    public Optional<CustomWandData> from(DataContainer container) {
        return container.getBoolean(PenguinDungeonKeys.IS_PD_WAND.getQuery()).map(CustomWandData::new);
    }

    @Override
    public CustomWandData copy() {
        return new CustomWandData(this.getValue());
    }

    @Override
    public ImmutableCustomWandData asImmutable() {
        return new ImmutableCustomWandData(this.getValue());
    }

    @Override
    public int getContentVersion() {
        return 1;
    }
}
