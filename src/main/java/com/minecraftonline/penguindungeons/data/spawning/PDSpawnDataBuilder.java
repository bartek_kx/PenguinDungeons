package com.minecraftonline.penguindungeons.data.spawning;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.manipulator.DataManipulatorBuilder;
import org.spongepowered.api.data.persistence.AbstractDataBuilder;
import org.spongepowered.api.data.persistence.InvalidDataException;

import java.util.Optional;

public class PDSpawnDataBuilder extends AbstractDataBuilder<PDSpawnData> implements DataManipulatorBuilder<PDSpawnData, ImmutablePDSpawnData>
{
    public PDSpawnDataBuilder()
    {
        super(PDSpawnData.class, 1);
    }

    @Override
    public PDSpawnData create()
    {
        return new PDSpawnData(false);
    }

    @Override
    public Optional<PDSpawnData> createFrom(DataHolder dataHolder)
    {
        return this.create().fill(dataHolder);
    }

    @Override
    protected Optional<PDSpawnData> buildContent(DataView container) throws InvalidDataException
    {
        if (container.contains(PenguinDungeonKeys.PD_SPAWN.getQuery()))
        {
            return Optional.of(new PDSpawnData(container.getBoolean(PenguinDungeonKeys.PD_SPAWN.getQuery()).get()));
        }

        return Optional.empty();
    }
}

