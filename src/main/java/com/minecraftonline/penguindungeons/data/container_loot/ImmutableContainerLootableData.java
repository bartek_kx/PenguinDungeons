package com.minecraftonline.penguindungeons.data.container_loot;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.manipulator.immutable.common.AbstractImmutableSingleData;
import org.spongepowered.api.data.value.immutable.ImmutableValue;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;

public class ImmutableContainerLootableData extends AbstractImmutableSingleData<Long, ImmutableContainerLootableData, ContainerLootableData>
{
	public ImmutableContainerLootableData(Long value)
	{
		super(PenguinDungeonKeys.CONTAINER_LOOTABLE, value);
	}

	@Override
    protected ImmutableValue<Long> getValueGetter() {
        return Sponge.getRegistry().getValueFactory()
                .createValue(PenguinDungeonKeys.CONTAINER_LOOTABLE, getValue())
                .asImmutable();
    }

    @Override
    public ContainerLootableData asMutable()
    {
        return new ContainerLootableData(this.getValue());
    }

    @Override
    public int getContentVersion()
    {
        return 1;
    }

}
