package com.minecraftonline.penguindungeons.data.container_loot;

import com.minecraftonline.penguindungeons.data.PenguinDungeonKeys;
import com.minecraftonline.penguindungeons.util.ResourceKey;

import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.data.DataView;
import org.spongepowered.api.data.manipulator.DataManipulatorBuilder;
import org.spongepowered.api.data.persistence.AbstractDataBuilder;
import org.spongepowered.api.data.persistence.InvalidDataException;

import java.util.Optional;

public class ContainerLootDataBuilder extends AbstractDataBuilder<ContainerLootData> implements DataManipulatorBuilder<ContainerLootData, ImmutableContainerLootData>
{
    public ContainerLootDataBuilder()
    {
        super(ContainerLootData.class, 1);
    }

    @Override
    public ContainerLootData create()
    {
        return new ContainerLootData(ResourceKey.of("dummy", "dummy"));
    }

    @Override
    public Optional<ContainerLootData> createFrom(DataHolder dataHolder)
    {
        return this.create().fill(dataHolder);
    }

    @Override
    protected Optional<ContainerLootData> buildContent(DataView container) throws InvalidDataException
    {
        if (container.contains(PenguinDungeonKeys.CONTAINER_LOOT.getQuery()))
        {
            String str = container.getString(PenguinDungeonKeys.CONTAINER_LOOT.getQuery()).get();
            final ResourceKey key;
            try {
                key = ResourceKey.resolve(str);
            } catch (IllegalArgumentException e) {
                throw new InvalidDataException("Invalid ResourceKey", e);
            }
            return Optional.of(new ContainerLootData(key));
        }
        return Optional.empty();
    }
}

