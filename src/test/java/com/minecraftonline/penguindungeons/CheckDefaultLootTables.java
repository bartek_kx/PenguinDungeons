package com.minecraftonline.penguindungeons;


import com.google.common.io.Resources;
import com.google.gson.Gson;
import com.minecraftonline.penguindungeons.customentity.CustomEntityType;
import com.minecraftonline.penguindungeons.customentity.CustomEntityTypes;
import com.minecraftonline.penguindungeons.customentity.StoredNBTEntityType;

import net.minecraft.init.Bootstrap;
import net.minecraft.util.JsonUtils;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraft.world.storage.loot.LootTableManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.stream.Collectors;

@RunWith(Parameterized.class)
public class CheckDefaultLootTables {

    private static final Gson GSON_INSTANCE = getGsonInstance();
    private static final String LOOT_TABLE_DIR = "/assets/penguindungeons/loottable/";

    private final CustomEntityType customEntity;

    public CheckDefaultLootTables(final String name, final CustomEntityType customEntity) {
        this.customEntity = customEntity;
    }

    private static Gson getGsonInstance() {
        for (Field field : LootTableManager.class.getDeclaredFields()) {
            if (Gson.class.equals(field.getType())) {
                try {
                    field.setAccessible(true);
                    return (Gson) field.get(null);
                } catch (IllegalAccessException e) {
                    throw new IllegalStateException("Failed to get json instance", e);
                }
            }
        }
        throw new IllegalStateException("Failed to find Gson declared field");
    }

    @Test
    public void checkLootTablesExistAndAreValid() {
        final String fullPath = getPath(this.customEntity);
        final URL url = CheckDefaultLootTables.class.getResource(fullPath);
        Assert.assertNotNull("Missing loot table for custom entity " + this.customEntity.getId() + " (path " + fullPath + ")", url);
        final String s;
        try {
            s = Resources.toString(url, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to read loot table file for custom entity");
        }
        JsonUtils.gsonDeserialize(CheckDefaultLootTables.GSON_INSTANCE, s, LootTable.class);
    }

    @Parameterized.Parameters(name="{0}")
    public static Collection<Object[]> parameters() {
        Bootstrap.register();
        CustomEntityTypes.initRegistry();
        return CustomEntityTypes.getAll().stream()
                .filter(type -> !(type instanceof StoredNBTEntityType))
                .map(type -> new Object[] {type.getId().toString(), type})
                .collect(Collectors.toList());
    }

    public static String getPath(CustomEntityType customEntityType) {
        return LOOT_TABLE_DIR + customEntityType.getId().formattedWith("/") + ".json";
    }
}
